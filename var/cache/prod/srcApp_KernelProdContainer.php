<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerNVcWMqS\srcApp_KernelProdContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerNVcWMqS/srcApp_KernelProdContainer.php') {
    touch(__DIR__.'/ContainerNVcWMqS.legacy');

    return;
}

if (!\class_exists(srcApp_KernelProdContainer::class, false)) {
    \class_alias(\ContainerNVcWMqS\srcApp_KernelProdContainer::class, srcApp_KernelProdContainer::class, false);
}

return new \ContainerNVcWMqS\srcApp_KernelProdContainer([
    'container.build_hash' => 'NVcWMqS',
    'container.build_id' => '4d82421a',
    'container.build_time' => 1575116771,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerNVcWMqS');
