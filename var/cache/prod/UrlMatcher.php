<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'home_page', '_controller' => 'App\\Controller\\DefaultController::showHomePage'], null, null, null, false, false, null]],
        '/home' => [[['_route' => 'home_connected', '_controller' => 'App\\Controller\\DefaultController::showConnectedHomePage'], null, null, null, false, false, null]],
        '/newad' => [[['_route' => 'new_ad', '_controller' => 'App\\Controller\\DefaultController::newAd'], null, null, null, false, false, null]],
        '/profil' => [[['_route' => 'profil_page', '_controller' => 'App\\Controller\\DefaultController::showProfilPage'], null, null, null, false, false, null]],
        '/repair' => [[['_route' => 'to_repair', '_controller' => 'App\\Controller\\DefaultController::showRepairPage'], null, null, null, false, false, null]],
        '/sell' => [[['_route' => 'to_sell', '_controller' => 'App\\Controller\\DefaultController::showSellPage'], null, null, null, false, false, null]],
        '/adsSaved' => [[['_route' => 'ads_saved', '_controller' => 'App\\Controller\\DefaultController::showAdsSaved'], null, null, null, false, false, null]],
        '/selectRepairer' => [[['_route' => 'select_repairer', '_controller' => 'App\\Controller\\DefaultController::showSelectRepairer'], null, null, null, false, false, null]],
        '/contact/toRepair' => [[['_route' => 'contact_repair', '_controller' => 'App\\Controller\\DefaultController::showContactRepair'], null, null, null, false, false, null]],
        '/contact/seller/buy' => [[['_route' => 'contact_seller_without_repair', '_controller' => 'App\\Controller\\DefaultController::showContactSellerBuy'], null, null, null, false, false, null]],
        '/contact/seller/repair' => [[['_route' => 'contact_seller_with_repair', '_controller' => 'App\\Controller\\DefaultController::showContactSellerWithRepair'], null, null, null, false, false, null]],
        '/contact/seller/repair-2' => [[['_route' => 'contact_seller_with_repair2', '_controller' => 'App\\Controller\\DefaultController::showContactSellerWithRepair2'], null, null, null, false, false, null]],
        '/messaging' => [[['_route' => 'messaging', '_controller' => 'App\\Controller\\DefaultController::showMessaging'], null, null, null, false, false, null]],
        '/edit-product' => [[['_route' => 'edit_product', '_controller' => 'App\\Controller\\DefaultController::showEditProduct'], null, null, null, false, false, null]],
        '/cgu' => [[['_route' => 'cgu', '_controller' => 'App\\Controller\\DefaultController::showCGU'], null, null, null, false, false, null]],
        '/contacter' => [[['_route' => 'contact', '_controller' => 'App\\Controller\\DefaultController::showContact'], null, null, null, false, false, null]],
        '/mentions_legales' => [[['_route' => 'legal_mentions', '_controller' => 'App\\Controller\\DefaultController::showLegalMentions'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
    ],
    [ // $dynamicRoutes
    ],
    null, // $checkCondition
];
