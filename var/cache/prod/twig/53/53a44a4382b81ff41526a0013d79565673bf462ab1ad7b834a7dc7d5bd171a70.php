<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_ace222dee269c6f9396e292c9a24b968d7ad1f1a48ddb9ec7a7a3249a94974cf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>
      ";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        // line 9
        echo "    </title>
    ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 18
        echo "  </head>
  <body>
    <section class=\"hero\">
      <!-- Page head -->
      <div class=\"hero-head\">
        <!-- Navbar -->
        ";
        // line 24
        $this->displayBlock('navbar', $context, $blocks);
        // line 78
        echo "      </div>
    </section>
    
    <section class=\"hero is-fullheight-with-navbar\">
      <div class=\"hero-body verticaly-top\">
        ";
        // line 83
        $this->displayBlock('body', $context, $blocks);
        // line 84
        echo "      </div>
      <!-- Footer -->
      <div class=\"hero-footer\">
        ";
        // line 87
        $this->displayBlock('footer', $context, $blocks);
        // line 122
        echo "      </div>
    </section>

    ";
        // line 125
        $this->displayBlock('javascripts', $context, $blocks);
        // line 136
        echo "  </body>
</html>
";
    }

    // line 8
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "UFiX ";
    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/app.css"), "html", null, true);
        echo "\" />
    <link
      rel=\"icon\"
      type=\"image/png\"
      href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon-onglet.png"), "html", null, true);
        echo "\"
    />
    ";
    }

    // line 24
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "        <nav class=\"navbar\" role=\"navigation\" aria-label=\"main navigation\">
          <div class=\"navbar-brand\">
            <a class=\"navbar-item\" href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_page");
        echo "\">
              <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo_white.png"), "html", null, true);
        echo "\" alt=\"Logo UFix\"/>
            </a>
            <a
              role=\"button\"
              class=\"navbar-burger burger\"
              aria-label=\"menu\"
              aria-expanded=\"false\"
              data-target=\"navbarBasicExample\"
            >
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
            </a>
          </div>

          <div id=\"navbarBasicExample\" class=\"navbar-menu\">
            <div class=\"navbar-center is-size-6\">
              <a class=\"navbar-item\" href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_ad");
        echo "\">
                Déposer une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_connected");
        echo "\">
                Rechercher une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_product");
        echo "\">
                Mes annonces
              </a>
            </div>
            <div class=\"navbar-center-mobile is-hidden-desktop\"></div>
            <div class=\"navbar-end\">
              <a class=\"navbar-item\" href=\"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_page");
        echo "\">
                <span class=\"icon is-medium\">
                  <i class=\"far fa-user is-size-6\"></i>
                </span>
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("messaging");
        echo "\">
                <span
                  class=\"icon is-small-desktop is-medium-mobile is-medium-tablet\"
                >
                  <i class=\"far fa-comment-alt is-size-6\"></i>
                </span>
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_saved");
        echo "\">
                <span class=\"icon is-medium\">
                  <i class=\"far fa-heart is-size-6\"></i>
                </span>
              </a>
            </div>
          </div>
        </nav>
        ";
    }

    // line 83
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 87
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 88
        echo "        <div class=\"columns is-vcentered\">
          <div class=\"column\">
            <p
              class=\"has-text-left-desktop has-text-centered-mobile has-text-left-tablet\"
            >
              <a href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("legal_mentions");
        echo "\" class=\"is-size-7\">Mentions légales</a>
              |
              <a href=\"";
        // line 95
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact");
        echo "\" class=\"is-size-7\">Nous contater</a>
              |
              <a href=\"";
        // line 97
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cgu");
        echo "\" class=\"is-size-7\">CGU</a>
            </p>
          </div>
          <div class=\"column\">
            <p class=\"has-text-centered is-size-7\">@UFiX 2019</p>
          </div>
          <div class=\"column\">
            <p
              class=\"has-text-right-desktop has-text-centered-mobile is-size-7 has-text-right-tablet\"
            >
              Retrouvez-nous sur :
              <a href=\"https://twitter.com/UFixFR\" class=\"icon-reseaux\">
                <span class=\"icon is-medium\">
                  <i class=\"fab fa-twitter\"></i>
                </span>
              </a>
              <a href=\"https://www.facebook.com/uFixFR/\" class=\"icon-reseaux\">
                <span class=\"icon is-small\">
                  <i class=\"fab fa-facebook-f\"></i>
                </span>
              </a>
            </p>
          </div>
        </div>
        ";
    }

    // line 125
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 126
        echo "    <script
      type=\"text/javascript\"
      src=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js\"
    ></script>
    <script
      type=\"text/javascript\"
      src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js\"
    ></script>
    <script type=\"text/javascript\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  278 => 134,  268 => 126,  264 => 125,  235 => 97,  230 => 95,  225 => 93,  218 => 88,  214 => 87,  208 => 83,  195 => 69,  185 => 62,  177 => 57,  168 => 51,  162 => 48,  156 => 45,  136 => 28,  132 => 27,  128 => 25,  124 => 24,  117 => 15,  109 => 11,  105 => 10,  98 => 8,  92 => 136,  90 => 125,  85 => 122,  83 => 87,  78 => 84,  76 => 83,  69 => 78,  67 => 24,  59 => 18,  57 => 10,  54 => 9,  52 => 8,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/base.html.twig");
    }
}
