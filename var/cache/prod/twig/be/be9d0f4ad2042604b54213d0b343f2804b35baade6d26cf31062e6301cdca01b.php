<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact.html.twig */
class __TwigTemplate_a9faecc9464d439a58e8ea63233925823406c7b69fd9833880b7415f2279f1e5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "contact.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Nous Contacter
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "\t";
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"title has-text-centered\">Contacter</p>
\t</div>
\t<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
\t\t<button class=\"delete\"></button>
\t\t<p>Message envoyé !</p>
\t</div>
";
    }

    // line 18
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "\t<div class=\"container has-text-centered\">
\t\t<div class=\"columns is-8 is-variable is-vcentered \">
\t\t\t<!-- Left part -->
\t\t\t<div class=\"column is-seven  is-centered\">
\t\t\t\t<h1 class=\"title is-1\">Contactez-nous</h1>
\t\t\t\t<p class=\"is-size-4\">
\t\t\t\t\tNous serons ravis de répondre à vos questions et à vos remarques.
\t\t\t\t</p>
\t\t\t\t<div class=\"social-media\">
\t\t\t\t\t<a href=\"https://www.facebook.com/uFixFR/\" class=\"icon\">
\t\t\t\t\t\t<i class=\"fab fa-facebook-square fa-3x\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"https://twitter.com/UFixFR\" class=\"icon\">
\t\t\t\t\t\t<i class=\"fab fa-twitter-square fa-3x\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- Form -->
\t\t\t<div class=\"column is-5 has-text-left margin-form\">
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Nom</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<input class=\"input is-medium\" type=\"text\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Email</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<input class=\"input is-medium\" type=\"text\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Message</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<textarea class=\"textarea is-medium\"></textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"control has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-primary \" href=\"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_page");
        echo "\" onclick=\"isSend()\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>\t
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 57,  81 => 19,  77 => 18,  63 => 8,  59 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "contact.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/contact.html.twig");
    }
}
