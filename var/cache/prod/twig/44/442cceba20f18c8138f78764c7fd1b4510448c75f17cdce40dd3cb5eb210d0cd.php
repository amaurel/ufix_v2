<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* toRepair.html.twig */
class __TwigTemplate_6aad39b777071291077ab9ec49bbd15389da915d0d3ed0e08e114c641b6c075a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "toRepair.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Annonce
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Annonce</p>
</div>
";
    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "<div class=\"container\">
\t<div class=\"card\">
\t\t<!-- Card header-->
\t\t<div class=\"card-header p-3t\">
\t\t\t<div class=\"columns\">
\t\t\t\t<div class=\"column is-half is-offset-one-quarter has-text-centered column-without-p-top\">
\t\t\t\t\t<div class=\"columns\">
\t\t\t\t\t\t<div class=\"column\">
\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"slider\">
\t\t\t\t\t\t<ul class=\"js__slider__images slider__images\">
\t\t\t\t\t\t\t<li class=\"slider__images-item\"><img class=\"slider__images-image\"
\t\t\t\t\t\t\t\t\tsrc=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"Image Iphone 4 écran cassé\" /></li>
\t\t\t\t\t\t\t<li class=\"slider__images-item\"><img class=\"slider__images-image\"
\t\t\t\t\t\t\t\t\tsrc=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone-go.jpg"), "html", null, true);
        echo "\" alt=\"Image Iphone 4 écran cassé\" /></li>
\t\t\t\t\t\t\t<li class=\"slider__images-item\"><img class=\"slider__images-image\"
\t\t\t\t\t\t\t\t\tsrc=\"https://unsplash.it/800/450?image=1026\" alt=\"Image seulement blanche\" /></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"slider__controls\">
\t\t\t\t\t\t\t<span class=\"slider__control js__slider__control--prev slider__control--prev\">
\t\t\t\t\t\t\t\t<</span> <ol class=\"js__slider__pagers slider__pagers\">
\t\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t\t\t<span
\t\t\t\t\t\t\t\t\t\tclass=\"slider__control js__slider__control--next slider__control--next\">></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<hr class=\"is-hidden-mobile\" />
\t\t<!-- Content -->
\t\t<div class=\"card-content\">
\t\t\t<div class=\"content\">
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<!-- Column onformation -->
\t\t\t\t\t<div class=\"column is-2-desktop is-hidden-mobile\"></div>
\t\t\t\t\t<div class=\"column is-3\">
\t\t\t\t\t\t<p class=\"subtitle is-uppercase\">Description</p>
\t\t\t\t\t\t<p>Iphone 5 avec un problème matériel, le téléphone s'allume et se charge mais reste bloqué sur
\t\t\t\t\t\t\tl'écran de recherche du serveur d'activation d'Apple</p>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- Column specification-->
\t\t\t\t\t<div class=\"column is-4\">
\t\t\t\t\t\t<p class=\"subtitle is-uppercase\">Détails Techniques</p>
\t\t\t\t\t\t<table class=\"has-text-centered\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th>marque</th>
\t\t\t\t\t\t\t\t\t<th>modèle</th>
\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Apple</td>
\t\t\t\t\t\t\t\t\t<td>Iphone 5</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th>couleur</th>
\t\t\t\t\t\t\t\t\t<th>capacité</th>
\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Blanc</td>
\t\t\t\t\t\t\t\t\t<td>64 Go</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- Column seller -->
\t\t\t\t\t<div class=\"column is-3\">
\t\t\t\t\t\t<p class=\"subtitle is-uppercase\">Vendeur</p>
\t\t\t\t\t\t<span class=\"tag orange\">4/5</span>
\t\t\t\t\t\t<p>Pascal MOLIPOU
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\tPessac - 33 600</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<!-- Card footer -->
\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<a href=\"";
        // line 107
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_saved");
        echo "\" class=\"save-ad\">
\t\t\t\t\t\t<i class=\"far fa-heart fa-2x\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"";
        // line 112
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_repair");
        echo "\">
\t\t\t\t\t\t<span>Réparer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t</div>
</div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "toRepair.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 112,  175 => 107,  97 => 32,  92 => 30,  75 => 15,  71 => 14,  62 => 8,  58 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "toRepair.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/toRepair.html.twig");
    }
}
