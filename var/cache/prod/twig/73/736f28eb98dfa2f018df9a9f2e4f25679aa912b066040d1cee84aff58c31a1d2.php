<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* edit_product.html.twig */
class __TwigTemplate_1c5221978263b4b8876f723d06fe5ace4627b4b9defd3cd6fe310cf19b8a9145 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "edit_product.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Mes annonces
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Mes annonces</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
  <button class=\"delete\"></button>
  <p>Annonce modifiée !</p>
</div>

";
    }

    // line 17
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "
<div class=\"container\">
  <div class=\"columns\">
    <!--  Left part -->
    <div class=\"column is-3-desktop is-half-mobile is-offset-one-quarter-mobile column-edit\">
      <article>
        <img id=\"image-new-ad\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"image annonce 1\" />
        <div class=\"text-block has-text-centered\">
          <p class=\"title\">Iphone 4</p>
          <p class=\"subtitle\">Ecran cassé</p>
        </div>
        <div class=\"has-text-centered\" id=\"selected\">
          MODIFIER
        </div>
      </article>
      <article>
        <img id=\"image-new-ad\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\" alt=\"image annonce 2\" />
        <div class=\"text-block has-text-centered\">
          <p class=\"title\">Iphone 4</p>
          <p class=\"subtitle\">Hors service</p>
        </div>
        <div class=\"has-text-centered\">
          MODIFIER
        </div>
      </article>
      <article>
        <img id=\"image-new-ad\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\" alt=\"image annonce 3\" />
        <div class=\"text-block has-text-centered\">
          <p class=\"title\">Iphone 6</p>
          <p class=\"subtitle\">Lightning foutue</p>
        </div>
        <div class=\"has-text-centered\">
          MODIFIER
        </div>
      </article>
    </div>

    <!-- Right part -->
    <div class=\"column is-9 column-without-p-top\">
      <div class=\"columns container2 is-multiline is-desktop p-3t\">
        <!-- Informtion part -->
        <div class=\"column is-9-desktop is-12\">
          <article class=\"tile is-child \">
            <p class=\"title has-text-centered\">Informations produit</p>
            <div class=\"field\">
              <label class=\"label\">Catégorie</label>
              <div class=\"control\">
                <div class=\"select\">
                  <select>
                    <option>Téléphone</option>
                    <option>Tablette</option>
                    <option>Ordinateur</option>
                  </select>
                </div>
              </div>
            </div>
            <div class=\"field\">
              <label class=\"label\">Nom du produit</label>
              <div class=\"control\">
                <input class=\"input\" type=\"text\" value=\"Iphone 4\" required />
              </div>
            </div>
            <div class=\"field\">
              <label class=\"label\">Etat du produit</label>
              <div class=\"control\">
                <input class=\"input\" type=\"text\" value=\"Ecran cassé\" required />
              </div>
            </div>

            <div class=\"field\">
              <label class=\"label\">Description</label>
              <div class=\"control\">
                <textarea class=\"textarea\" required>
Je vend mon iphone 4, l'écran est cassé mais le tactile fonctionne, le bouton home est capricieux.
A réparer ou bien pour pièces, prix ferme. 
                        </textarea>
              </div>
            </div>

            <div class=\"field\">
              <div class=\"control\">
                <div class=\"field\">
                  <input class=\"is-checkradio is-rtl has-background-color is-white\" id=\"exampleRtlRadioInline1\"
                    type=\"radio\" name=\"exampleRtlRadioInline\" checked=\"checked\" />
                  <label for=\"exampleRtlRadioInline1\">Vendre</label>
                  <input class=\"is-checkradio is-rtl has-background-color is-white\" id=\"exampleRtlRadioInline2\"
                    type=\"radio\" name=\"exampleRtlRadioInline\" />
                  <label for=\"exampleRtlRadioInline2\">Réparer</label>
                </div>
              </div>
            </div>

            <div class=\"field prix\">
              <label class=\"label\">Prix</label>
              <div class=\"control has-icons-right\">
                <input class=\"input\" type=\"text\" value=\"49.99\" required />
                <span class=\"icon is-small is-right\">
                  <i class=\"fas fa-euro-sign\"></i>
                </span>
              </div>
            </div>

          </article>
        </div>
        <!-- Images to add -->
        <div class=\"column is-3-desktop is-8-mobile is-offset-2-mobile\">
          <p class=\"title has-text-centered\">Images produit</p>
          ";
        // line 126
        echo "          <div class=\"avatar-upload\">
            <div class=\"avatar-edit\">
              <input type=\"file\" id=\"imageUpload\" accept=\".png, .jpg, .jpeg\" />
              <label for=\"imageUpload\"></label>
            </div>
            <div class=\"avatar-preview\">
              <div id=\"imagePreview\" style=\"background-image: url('";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "');\"></div>
            </div>
          </div>
          ";
        // line 136
        echo "          <div class=\"avatar-upload\">
            <div class=\"avatar-edit\">
              <input type=\"file\" id=\"imageUpload2\" accept=\".png, .jpg, .jpeg\" />
              <label for=\"imageUpload2\"></label>
            </div>
            <div class=\"avatar-preview\">
              <div id=\"imagePreview2\"></div>
            </div>
          </div>
          ";
        // line 146
        echo "          <div class=\"avatar-upload\">
            <div class=\"avatar-edit\">
              <input type=\"file\" id=\"imageUpload3\" accept=\".png, .jpg, .jpeg\" />
              <label for=\"imageUpload3\"></label>
            </div>
            <div class=\"avatar-preview\">
              <div id=\"imagePreview3\"></div>
            </div>
          </div>
        </div>
        <!-- Submit -->
        <div class=\"column is-half is-offset-one-quarter has-text-centered\">
          <a class=\"button is-primary is-rounded is-outlined\" href=\"";
        // line 158
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_connected");
        echo "\" onclick=\"isSend()\">
            <span>Modifier</span>
            <span class=\"icon\">
              <i class=\"fas fa-angle-right\"></i>
            </span>
          </a>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "edit_product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 158,  224 => 146,  213 => 136,  207 => 132,  199 => 126,  115 => 44,  102 => 34,  89 => 24,  81 => 18,  77 => 17,  63 => 8,  59 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "edit_product.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/edit_product.html.twig");
    }
}
