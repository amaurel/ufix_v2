<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* profil.html.twig */
class __TwigTemplate_4696cb4e6d0a7aeca5493c6f8189ef4dc8833b41f34ca3af4e5484d5bc8d9069 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "profil.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
\t- Mon profil
";
    }

    // line 8
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "\t";
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"title has-text-centered\">Profil</p>
\t</div>
\t<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
\t\t<button class=\"delete\"></button>
\t\t<p>Profil modifiée !</p>
\t</div>
";
    }

    // line 18
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "\t<div class=\"container p-3t\">
\t\t<p class=\"title is-size-4 has-text-centered\">Pascal MOLIPOU</p><br/>
\t\t<div class=\"columns\">
\t\t\t<div
\t\t\t\tclass=\"column is-half is-offset-3\">
\t\t\t\t<!-- Profile avatar -->
\t\t\t\t<div class=\"avatar-upload2\">
\t\t\t\t\t<div class=\"avatar-edit\">
\t\t\t\t\t\t<input type=\"file\" id=\"imageUpload\" accept=\".png, .jpg, .jpeg\"/>
\t\t\t\t\t\t<label for=\"imageUpload\"></label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"avatar-preview\">
\t\t\t\t\t\t<div id=\"imagePreview\" style=\"background-image: url('";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(" img/profil-default.jpg"), "html", null, true);
        echo "');\"></div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- Form -->
\t\t\t\t<form action=\"\" class=\"profil\">
\t\t\t\t\t<div class=\"field\">
\t\t\t\t\t\t<div class=\"control has-icons-left\">
\t\t\t\t\t\t\t<input class=\"input\" type=\"email\" value=\"pascal.molipou@gmail.com\" placeholder=\"Email\"/>
\t\t\t\t\t\t\t<span class=\"icon is-small is-left\">
\t\t\t\t\t\t\t\t<i class=\"fas fa-envelope\"></i>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"field\">
\t\t\t\t\t\t<div class=\"control has-icons-left\">
\t\t\t\t\t\t\t<input class=\"input\" type=\"text\" value=\"12 rue du palier\" placeholder=\"Adresse\"/>
\t\t\t\t\t\t\t<span class=\"icon is-small is-left\">
\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker\"></i>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"field\">
\t\t\t\t\t\t<div class=\"control has-icons-left\">
\t\t\t\t\t\t\t<input class=\"input\" type=\"text\" value=\"21000\" placeholder=\"Code postal\"/>
\t\t\t\t\t\t\t<span class=\"icon is-small is-left\">
\t\t\t\t\t\t\t\t<i class=\"fas fa-map-marker\"></i>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"field\">
\t\t\t\t\t\t<div class=\"control has-icons-left\">
\t\t\t\t\t\t\t<input class=\"input\" type=\"text\" value=\"Dijon\" placeholder=\"Ville\"/>
\t\t\t\t\t\t\t<span class=\"icon is-small is-left\">
\t\t\t\t\t\t\t\t<i class=\"fas fa-city\"></i>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"field m-2b\">
\t\t\t\t\t\t<div class=\"control has-icons-left\">
\t\t\t\t\t\t\t<input class=\"input\" type=\"text\" value=\"France\" placeholder=\"Pays\"/>
\t\t\t\t\t\t\t<span class=\"icon is-small is-left\">
\t\t\t\t\t\t\t\t<i class=\"fas fa-globe\"></i>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
          <!-- Submit -->
\t\t\t\t\t<div class=\"has-text-centered\">
\t\t\t\t\t\t<a class=\"button is-outlined is-rounded is-primary\" href=\"";
        // line 79
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_connected");
        echo "\" onclick=\"isSend()\">
\t\t\t\t\t\t\t<span>CONFIRMER</span>
\t\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t</div>
";
    }

    public function getTemplateName()
    {
        return "profil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 79,  96 => 31,  82 => 19,  78 => 18,  64 => 9,  60 => 8,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "profil.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/profil.html.twig");
    }
}
