<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ads_saved.html.twig */
class __TwigTemplate_0fa7beb240c045178399cf599e4402dff2a780a68cb5c385bebe68e12b3ac8c1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "ads_saved.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Annonces sauvegardées
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Annonces sauvegardées</p>
</div>
";
    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "<div class=\"container is-fluid-without-padding p-3t\">
  <div class=\"columns is-centered\">
    <div class=\"column is-half\">
      <article class=\"panel\">
        <!-- Top of card -->
        <p class=\"panel-tabs\">
          <a class=\"is-active\">Tous</a>
          <a>Mobile</a>
          <a>Ordinateur</a>
          <a>Tablette</a>
        </p>
        <!-- Search -->
        <div class=\"panel-block\">
          <p class=\"control has-icons-left\">
            <input class=\"input is-dark\" type=\"text\" placeholder=\"Search\" />
            <span class=\"icon is-left\">
              <i class=\"fas fa-search\" aria-hidden=\"true\"></i>
            </span>
          </p>
        </div>
        <!-- Saved items -->
        <div class=\"panel-block\" id=\"ads_delete_1\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse-petit.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 6</span>
                    <br />
                    <span class=\"subtitle\">Lighting foutue</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete1()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_2\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ok3.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 5</span>
                    <br />
                    <span class=\"subtitle\">Ecran cassé</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete2()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_3\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 4</span>
                    <br />
                    <span class=\"subtitle\">Hors service</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 130
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete3()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_4\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 6</span>
                    <br />
                    <span class=\"subtitle\">Lighting foutue</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 167
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete4()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
      </article>
    </div>
  </div>
  <div class=\"columns\"></div>
</div>
";
    }

    public function getTemplateName()
    {
        return "ads_saved.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 167,  235 => 152,  210 => 130,  192 => 115,  167 => 93,  149 => 78,  124 => 56,  106 => 41,  76 => 13,  72 => 12,  63 => 8,  59 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "ads_saved.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/ads_saved.html.twig");
    }
}
