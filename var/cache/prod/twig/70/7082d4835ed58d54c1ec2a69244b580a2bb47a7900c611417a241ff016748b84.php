<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* toSell.html.twig */
class __TwigTemplate_f7bf869d70429ee574af989b29c7415fe7d9eb24fd9f634130d81fa6d2fd0687 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "toSell.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Annonce
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Annonce</p>
</div>
";
    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "
<div class=\"container\">
\t<div class=\"card\">
\t\t<!-- Card header -->
\t\t<div class=\"card-header p-3t\">
\t\t\t<div class=\"columns\">
\t\t\t\t<div class=\"column is-half is-offset-one-quarter has-text-centered column-without-p-top\">
\t\t\t\t\t<div class=\"columns\">
\t\t\t\t\t\t<div class=\"column \">
\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 29
        echo "\t\t\t\t\t<div class=\"slider\">
\t\t\t\t\t\t<ul class=\"js__slider__images slider__images\">
\t\t\t\t\t\t\t<li class=\"slider__images-item\"><img class=\"slider__images-image\"
\t\t\t\t\t\t\t\t\tsrc=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"Image Iphone 4 écran cassé\" /></li>
\t\t\t\t\t\t\t<li class=\"slider__images-item\"><img class=\"slider__images-image\"
\t\t\t\t\t\t\t\tsrc=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone-go.jpg"), "html", null, true);
        echo "\" alt=\"Image Iphone 4 écran cassé\" /></li>
\t\t\t\t\t\t\t<li class=\"slider__images-item\"><img class=\"slider__images-image\"
\t\t\t\t\t\t\t\t\tsrc=\"https://unsplash.it/800/450?image=1026\" alt=\"Image seulement blanche\"/></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"slider__controls\">
\t\t\t\t\t\t\t<span class=\"slider__control js__slider__control--prev slider__control--prev\"><</span>
\t\t\t\t\t\t\t<ol class=\"js__slider__pagers slider__pagers\"></ol>
\t\t\t\t\t\t\t<span class=\"slider__control js__slider__control--next slider__control--next\">></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<hr class=\"is-hidden-mobile\" />

\t\t<!-- Content -->
\t\t<div class=\"card-content\">
\t\t\t<div class=\"content\">
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<!-- Column onformation -->
\t\t\t\t\t<div class=\"column is-2-desktop is-hidden-mobile\"></div>
\t\t\t\t\t<div class=\"column is-3\">
\t\t\t\t\t\t<p class=\"subtitle is-uppercase\">Description</p>
\t\t\t\t\t\t<p>Je vend mon Iphone 4, l'écran est cassé mais le tactile fonctionne, le bonton home est
\t\t\t\t\t\t\tcapricieux. A réparer ou bien pour pièces, prix ferme.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- Column specification-->
\t\t\t\t\t<div class=\"column is-4\">
\t\t\t\t\t\t<p class=\"subtitle is-uppercase\">Détails Techniques</p>
\t\t\t\t\t\t<table class=\"has-text-centered\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th>marque</th>
\t\t\t\t\t\t\t\t\t<th>modèle</th>
\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Apple</td>
\t\t\t\t\t\t\t\t\t<td>Iphone 4</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th>couleur</th>
\t\t\t\t\t\t\t\t\t<th>capacité</th>
\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>Noir</td>
\t\t\t\t\t\t\t\t\t<td>64 Go</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- Column seller -->
\t\t\t\t\t<div class=\"column is-3\">
\t\t\t\t\t\t<p class=\"subtitle is-uppercase\">Vendeur</p>
\t\t\t\t\t\t<span class=\"tag orange\">4/5</span>
\t\t\t\t\t\t<p>Jacques POMMIER
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\tGradignan - 33 170</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<!-- Card footer-->
\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-2 has-text-centered\">
\t\t\t\t\t<p>50€</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-2 has-text-centered\">
\t\t\t\t\t<a href=\"";
        // line 111
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_saved");
        echo "\" class=\"save-ad\">
\t\t\t\t\t\t<i class=\"far fa-heart fa-2x\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"";
        // line 116
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("select_repairer");
        echo "\">
\t\t\t\t\t\t<span>Avec réparation</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-primary\"
\t\t\t\t\t\thref=\"";
        // line 125
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_seller_without_repair");
        echo "\">
\t\t\t\t\t\t<span>Contacter</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t</div>
</div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "toSell.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 125,  188 => 116,  180 => 111,  100 => 34,  95 => 32,  90 => 29,  75 => 15,  71 => 14,  62 => 8,  58 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "toSell.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/toSell.html.twig");
    }
}
