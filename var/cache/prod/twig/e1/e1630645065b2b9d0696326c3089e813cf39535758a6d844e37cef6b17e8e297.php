<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messaging.html.twig */
class __TwigTemplate_f9a1228012c47b2a28d3c8c7b831e426fa93ded70245de58207f790953d20b66 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'aside' => [$this, 'block_aside'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "messaging.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Messagerie
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Messagerie</p>
</div>
";
    }

    // line 12
    public function block_aside($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "
<div class=\"container\">
  <div class=\"card\">
    <div class=\"card-content\">
      <div class=\"content\">
        <div class=\"columns\">
          <div class=\"column is-2 is-fullheight contacts is-paddingless\">
          
            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 6));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 22
            echo "              <div class=\"contact\">
                <div class=\"columns\">
                  <div class=\"column\">
                    <div class=\"columns is-vcentered\">
                      <div class=\"column\">
                        <p>
                          <span class=\"title icon-before-msg\">Jacques Pommier</span>
                          <br/>
                          <span class=\"subtitle\">Vendeur</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr class=\"b-1b\"/>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "            
          </div>
          <div class=\"column is-10 is-paddingless chat\">
            <div class=\"columns has-text-centered\">
              <div class=\"column is-4 is-offset-4\">
                <p class=\"title\">Jacques Pommier</p>
                <p class=\"subtitle\">Vendeur</p>
              </div>
            </div>
            <!-- end chat-header -->

            <div class=\"chat-history\">
              <ul  class=\"margin-form\" id=\"messages\">
                <li class=\"clearfix\">
                  <div class=\"message-data has-text-right\">
                    <span class=\"message-data-time\">10:10 AM, Today</span>
                    &nbsp; &nbsp;
                    <span class=\"message-data-name\">Olia</span>
                  </div>
                  <div class=\"message other-message is-pulled-right\">
                      Quisque sagittis blandit nunc, vitae ultricies odio porttitor quis. Vivamus mattis urna orci.
                  </div>
                </li>

                <li>
                  <div class=\"message-data\">
                    <span class=\"message-data-name\"> Vincent</span>
                    <span class=\"message-data-time\">10:12 AM, Today</span>
                  </div>
                  <div class=\"message my-message\">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut varius, magna id imperdiet fermentum, nunc 
                      ipsum faucibus libero, non dictum leo felis ut quam. Ut vitae dui laoreet, convallis risus vulputate,
                  </div>
                </li>

                <li class=\"clearfix\">
                  <div class=\"message-data has-text-right\">
                    <span class=\"message-data-time\">10:14 AM, Today</span>
                    &nbsp; &nbsp;
                    <span class=\"message-data-name\">Olia</span>
                  </div>
                  <div class=\"message other-message is-pulled-right\">
                    Vivamus rhoncus cursus ligula vitae tincidunt. Nullam urna ex, congue ac semper nec, varius ac magna. Nulla facilisi. Sed ac leo eget purus blandit sodales nec sed ipsum. Maecenas dignissim ipsum lorem, nec aliquet risus semper eget. Vestibulum sit amet lectus et elit dictum porta. Curabitur ac laoreet neque. Pellentesque lobortis urna id eros 
                  </div>
                </li>

                <li>
                  <div class=\"message-data\">
                    <span class=\"message-data-name\"> Vincent</span>
                    <span class=\"message-data-time\">10:20 AM, Today</span>
                  </div>
                  <div class=\"message my-message\">
                      Sed eu ante euismod, sollicitudin libero eu, pretium tortor. Vivamus sit amet velit diam. Nam egestas tincidunt pharetra. Cras efficitur diam scelerisque felis congue, nec 
                  </div>
                </li>
              </ul>
            </div>
            <!-- end chat-history -->

            <div class=\"chat-message clearfix\">
              <textarea
                name=\"message-to-send\"
                id=\"message-to-send\"
                placeholder=\"Type your message\"
                rows=\"3\"
              ></textarea>

              <button
                class=\"button is-outlined is-rounded is-primary is-pulled-right\"
                id=\"send-message\"
                onclick=\"sendMessage()\"
              >
                <span>Envoyer</span>
                <span class=\"icon\">
                  <i class=\"fas fa-angle-right\"></i>
                </span>
              </button>
            </div>

            <!-- end chat-message -->

            <!-- end chat -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "messaging.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 39,  96 => 22,  92 => 21,  82 => 13,  73 => 12,  64 => 8,  60 => 7,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "messaging.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/messaging.html.twig");
    }
}
