<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home.html.twig */
class __TwigTemplate_e10e6ba56a87c393196e8320803e8aa09944c82419ada95656ac9af53d881bc5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base2.html.twig", "home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Accueil
";
    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<!-- Notification -->
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
  <button class=\"delete\"></button>
  <p>Vous êtes bien inscrit.</p>
</div>

<section class=\"hero is-fullheight-with-navbar\">
  <div class=\"hero-body\">
    <div class=\"container\">
      <!-- Top part -->
      <div class=\"columns is-vcentered p-3t\">
        <!-- Left part-->
        <div class=\"column is-5 is-offset-1-desktop is-offset-1-tablet is-offset-1-mobile landing-caption\">
          <h1 class=\"title is-1 is-bold is-spaced\">
            Faites renaître vos appareils de leurs cendres !
          </h1>
          <h2 class=\"subtitle is-5 is-dark\">Et ainsi participer à la réduction de l'empreinte environnementale.</h2>
          <p>
            <a class=\"button is-outlined is-primary scroll\" href=\"#third-block\">Connexion / Inscription</a>
          </p>
        </div>
        <!-- Image -->
        <div class=\"column is-5\">
          <img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/home.jpg"), "html", null, true);
        echo "\" alt=\"Image présentation UFix\" />
        </div>
      </div>
    </div>
  </div>
  <!-- Button to link next section -->
  <div class=\"hero-footer has-background-white\">
    <div class=\"container has-text-centered\">
      <a class=\"scroll\" href=\"#second-block\">
        <span class=\"icon\">
          <i class=\"fas fa-chevron-down\"></i>
        </span>
      </a>
    </div>
  </div>
</section>

<section id=\"second-block\" class=\"section is-medium\">
  <div class=\"container\">
    <!-- Presentation top-->
    <div class=\"title-wrapper has-text-centered\">
      <h2 class=\"title is-4\">Grâce à UFix tu peux :</h2>
      <h3 class=\"subtitle is-6\">*tout en préservant l'environnement</h3>
      <div class=\"divider is-centered\"></div>
    </div>
    <!-- Presentation of UFix -->
    <div class=\"content-wrapper\">
      <div class=\"columns\">
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Réparer</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon1.png"), "html", null, true);
        echo "\" alt=\"Icone réparer\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Tu souhaites réparer des appareils électroniques tels que des téléphones,
                ordinateurs ou tablettes ?</p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Réparer des appareils</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Acheter</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon2.png"), "html", null, true);
        echo "\" alt=\"Icone acheter\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Achètes des appareils électroniques usagés mais fonctionnels, à moindre coût.
              </p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Acheter un appareil</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Vendre</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon4.png"), "html", null, true);
        echo "\" alt=\"Icone vendre\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Vends tes appareils usagés et participe à la réduction de l'empreinte
                environnementale sur notre planète.</p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("newad"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Vendre un appareil</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Faire réparer</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon3.png"), "html", null, true);
        echo "\" alt=\"Icone faire réparer\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Viens faire réparer tes appareils cassés en toute simplicité et de façon
                responsable.</p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("newad"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Faire réparer son appareil</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Inscription and connexion part -->
<section id=\"third-block\" class=\"section is-medium\">
  <div class=\"container\">
    <div class=\"columns is-vcentered\">
      <div class=\"column is-8 is-offset-2\">
        <div class=\"columns shadows-effect\">
          <!-- Connexion -->
          <div class=\"column is-4 has-text-centered sign-in-home\">
            <p class=\"title is-light\">Connexion</p>
            <form method=\"post\" action=\"\" class=\"form p-form\">
              <div class=\"field\">
                <p class=\"control has-icons-left has-icons-right\">
                  <input class=\"input\" type=\"email\" placeholder=\"Email\" required>
                  <span class=\"icon is-small is-left\">
                    <i class=\"fas fa-envelope\"></i>
                  </span>
                  <span class=\"icon is-small is-right\">
                    <i class=\"fas fa-check\"></i>
                  </span>
                </p>
              </div>
              <div class=\"field m-2b\">
                <p class=\"control has-icons-left\">
                  <input class=\"input\" type=\"password\" placeholder=\"Password\" required>
                  <span class=\"icon is-small is-left\">
                    <i class=\"fas fa-lock\"></i>
                  </span>
                </p>
              </div>
              <button class=\"button is-primary is-outlined\" href=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\" type=\"submit\">Se
                connecter</button>
            </form>
          </div>
          <!-- Innscription -->
          <div class=\"column is-8 has-text-centered sign-up-home\">
            <p class=\"title has-text-centered\">Inscription</p>
            <form method=\"post\" action=\"\" class=\"form\">
              <div class=\"columns\">
                <div class=\"column is-6\">
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Nom\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-user\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Prénom\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-user\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"email\" placeholder=\"Email\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-envelope\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"password\" placeholder=\"Mot de passe\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-lock\"></i>
                      </span>
                    </p>
                  </div>
                </div>
                <div class=\"column is-6\">
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Adresse\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-map-marker-alt\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Code postale\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-map-marker-alt\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Pays\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-flag\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field m-2b\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"text\" placeholder=\"Ville\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-building\"></i>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <label class=\"checkbox\">
                <input type=\"checkbox\" required>
                I agree to the <a href=\"#\">terms and conditions</a>
              </label>
              <br />
              <button class=\"button is-light is-outlined m-1t\" href=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\"
                type=\"submit\">M'inscrire</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 243,  243 => 160,  203 => 123,  193 => 116,  180 => 106,  170 => 99,  157 => 89,  147 => 82,  134 => 72,  124 => 65,  87 => 31,  62 => 8,  58 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/home.html.twig");
    }
}
