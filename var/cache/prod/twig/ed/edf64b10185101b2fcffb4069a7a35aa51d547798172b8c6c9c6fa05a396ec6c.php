<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* select_repairer.html.twig */
class __TwigTemplate_78613e24841293b8e4b759fa61a5afee30123b09df8d385c6f956cebedad4ab2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "select_repairer.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Sélection du réparateur
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Sélection du réparateur</p>
</div>
";
    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "<div class=\"container\">
\t<div class=\"card\">
\t\t<div class=\"card-content\">
\t\t\t<div class=\"content\">
\t\t\t\t<!-- Top of content page -->
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<div class=\"column is-4 is-offset-4\">
\t\t\t\t\t\t<div class=\"columns\">
\t\t\t\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t\t\t\t<img id=\"image-new-ad\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"image produit\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- Content -->
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<!-- Left column -->
\t\t\t\t\t<div class=\"column is-half \">
\t\t\t\t\t\t<div class=\"container-is-fluid extended\">
\t\t\t\t\t\t\t<p class=\"title has-text-centered\">Sélectionner un réparateur</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"is-scrollable\">
\t\t\t\t\t\t\t";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 19));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 42
            echo "\t\t\t\t\t\t\t<a class=\"repairer is-fullwidth\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_seller_with_repair2");
            echo "\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<!-- Right column -->
\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t<div class=\"container-is-fluid extended\">
\t\t\t\t\t\t\t<p class=\"title has-text-centered\">Proposer une réparation</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"columns is-margin-top\">
\t\t\t\t\t\t\t<div class=\"column is-6 is-offset-3\">
\t\t\t\t\t\t\t\t<div class=\"columns has-text-centered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t\t\t\t\t<div class=\"field\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"input\" type=\"text\" placeholder=\"Votre prix\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
        // line 83
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_repair");
        echo "\">
\t\t\t\t\t\t\t\t\t\t\t<button class=\"button is-outlined is-rounded is-info\">Soumettre</button>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<!-- Bottom of the page -->
\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">49.99€</span>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">
\t\t\t\t\t\t\tproduit</span>
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">16.50€</span>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">
\t\t\t\t\t\t\tréparation</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<p class=\"title text-footer\">66.49€</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"";
        // line 116
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_seller_with_repair");
        echo "\">
\t\t\t\t\t\t<span>Contacter le réparateur et le vendeur</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t</div>
</div>


";
    }

    public function getTemplateName()
    {
        return "select_repairer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 116,  161 => 83,  141 => 65,  111 => 42,  107 => 41,  87 => 24,  76 => 15,  72 => 14,  63 => 8,  59 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "select_repairer.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/select_repairer.html.twig");
    }
}
