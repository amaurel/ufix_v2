<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base2.html.twig */
class __TwigTemplate_4a8c031e06fafea88e59034438775b9caa75a3f2ccc32f646379d16bee814ad5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "  </head>
  <body>
    
    <section class=\"hero\">
      <!-- page head-->
      <div class=\"hero-head\">

        <!-- navbar -->
        ";
        // line 24
        $this->displayBlock('navbar', $context, $blocks);
        // line 82
        echo "      </div>
    </section>

    ";
        // line 85
        $this->displayBlock('body', $context, $blocks);
        // line 86
        echo "
    <!-- Footer -->
    <section class=\"hero\">
      <div class=\"hero-footer\">
        ";
        // line 90
        $this->displayBlock('footer', $context, $blocks);
        // line 125
        echo "      </div>
    </section>

    ";
        // line 128
        $this->displayBlock('javascripts', $context, $blocks);
        // line 139
        echo "  </body>
</html>
";
    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "UFiX";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/app.css"), "html", null, true);
        echo "\" />
    <link
      rel=\"icon\"
      type=\"image/png\"
      href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon-onglet.png"), "html", null, true);
        echo "\"
    />
    ";
    }

    // line 24
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "        <nav class=\"navbar\" role=\"navigation\" aria-label=\"main navigation\">
          <div class=\"navbar-brand\">
            <a class=\"navbar-item\" href=\"#\">
              <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo_white.png"), "html", null, true);
        echo "\" alt=\"Logo UFix\"/>
            </a>

            <a
              role=\"button\"
              class=\"navbar-burger burger\"
              aria-label=\"menu\" 
              aria-expanded=\"false\"
              data-target=\"navbarBasicExample\"
            >
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
            </a>
          </div>

          <div id=\"navbarBasicExample\" class=\"navbar-menu\">
            <div class=\"navbar-center is-size-6\">
              <a class=\"navbar-item\" href=\"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_ad");
        echo "\">
                Déposer une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_connected");
        echo "\">
                Rechercher une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_product");
        echo "\">
                Mes annonces
              </a>
            </div>
            <div class=\"navbar-center-mobile is-hidden-desktop\"></div>

            <div class=\"navbar-end\">
              <div class=\"navbar-item\">
                <a class=\"navbar-item\" href=\"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_page");
        echo "\">
                  <span class=\"icon is-medium\">
                    <i class=\"far fa-user is-size-6\"></i>
                  </span>
                </a>
                <a class=\"navbar-item\" href=\"";
        // line 65
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("messaging");
        echo "\">
                  <span
                    class=\"icon is-small-desktop is-medium-mobile is-medium-tablet\"
                  >
                    <i class=\"far fa-comment-alt is-size-6\"></i>
                  </span>
                </a>
                <a class=\"navbar-item\" href=\"";
        // line 72
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_saved");
        echo "\">
                  <span class=\"icon is-medium\">
                    <i class=\"far fa-heart is-size-6\"></i>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </nav>
        ";
    }

    // line 85
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 90
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 91
        echo "        <div class=\"columns is-vcentered\">
          <div class=\"column\">
            <p
              class=\"has-text-left-desktop has-text-centered-mobile has-text-left-tablet\"
            >
              <a href=\"";
        // line 96
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("legal_mentions");
        echo "\" class=\"is-size-7\">Mentions légales</a>
              |
              <a href=\"";
        // line 98
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact");
        echo "\" class=\"is-size-7\">Nous contater</a>
              |
              <a href=\"";
        // line 100
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cgu");
        echo "\" class=\"is-size-7\">CGU</a>
            </p>
          </div>
          <div class=\"column\">
            <p class=\"has-text-centered is-size-7\">@UFiX 2019</p>
          </div>
          <div class=\"column\">
            <p
              class=\"has-text-right-desktop has-text-centered-mobile is-size-7 has-text-right-tablet\"
            >
              Retrouvez-nous sur :
              <a href=\"https://twitter.com/UFixFR\" class=\"icon-reseaux\">
                <span class=\"icon is-medium\">
                  <i class=\"fab fa-twitter\"></i>
                </span>
              </a>
              <a href=\"https://www.facebook.com/uFixFR/\" class=\"icon-reseaux\">
                <span class=\"icon is-small\">
                  <i class=\"fab fa-facebook-f\"></i>
                </span>
              </a>
            </p>
          </div>
        </div>
        ";
    }

    // line 128
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 129
        echo "    <script
      type=\"text/javascript\"
      src=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js\"
    ></script>
    <script
      type=\"text/javascript\"
      src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js\"
    ></script>
    <script type=\"text/javascript\" src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
    ";
    }

    public function getTemplateName()
    {
        return "base2.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  278 => 137,  268 => 129,  264 => 128,  235 => 100,  230 => 98,  225 => 96,  218 => 91,  214 => 90,  208 => 85,  194 => 72,  184 => 65,  176 => 60,  165 => 52,  159 => 49,  153 => 46,  132 => 28,  127 => 25,  123 => 24,  116 => 13,  108 => 9,  104 => 8,  97 => 7,  91 => 139,  89 => 128,  84 => 125,  82 => 90,  76 => 86,  74 => 85,  69 => 82,  67 => 24,  57 => 16,  55 => 8,  51 => 7,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base2.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/base2.html.twig");
    }
}
