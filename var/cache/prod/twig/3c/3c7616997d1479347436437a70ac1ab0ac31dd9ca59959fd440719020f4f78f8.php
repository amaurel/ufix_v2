<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contactSellerRepair2.html.twig */
class __TwigTemplate_35470d9cf9d228454810634e1b4ac1b3161459634bba2698671c63a8e381b1d1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base.html.twig", "contactSellerRepair2.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Contacter le réparateur
";
    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Contacter le réparateur</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
\t<button class=\"delete\"></button>
\t<p>Message envoyé !</p>
</div>
";
    }

    // line 17
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "<div class=\"container\">
\t<div class=\"card\">
\t\t<!-- Header -->
\t\t<div class=\"card-header p-3t\">
\t\t\t<div class=\"has-text-centered\">
\t\t\t\t<p class=\" title is-size-4\">Marie TYPOU</p>
\t\t\t\t<p class=\"title is-size-5 has-text-grey\">GRADIGNAN - 33170</p>
\t\t\t</div>
\t\t</div>

\t\t<!-- Content -->
\t\t<div class=\"card-content has-text-centered\">
\t\t\t<div class=\"content\">
\t\t\t\t<form action=\"\">
\t\t\t\t\t<div class=\"field margin-contact\">
\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t<textarea class=\"textarea min-height-textarea\" required>
Bonjour, 
              
Je suis intéressé par votre offre de réparation, j'ai pris contact avec le
vendeur du produit et si sa réponse est positive je le ferai réparer par vos
soir.

Cordialement,
Léo POMA
\t\t\t\t\t\t\t\t</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t<!-- Footer -->
\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Iphone 4</span>
\t\t\t\t\t\t<br />
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Ecran cassé</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"";
        // line 60
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("messaging");
        echo "\"
\t\t\t\t\t\tonclick=\"isSend()\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>

\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "contactSellerRepair2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 60,  80 => 18,  76 => 17,  63 => 8,  59 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "contactSellerRepair2.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/contactSellerRepair2.html.twig");
    }
}
