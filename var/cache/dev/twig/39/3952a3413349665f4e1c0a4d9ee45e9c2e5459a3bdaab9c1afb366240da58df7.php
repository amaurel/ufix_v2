<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* select_repairer.html.twig */
class __TwigTemplate_64a9cd0ac325866898e1656688b59e9fd2a3bb5a154c0bf8aeee09137a1520bf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "select_repairer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "select_repairer.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "select_repairer.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 3
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Sélection du réparateur</p>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "
<div class=\"container\">
\t<div class=\"card\">
\t\t<div class=\"card-content\">
\t\t\t<div class=\"content\">
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<div class=\"column is-4 is-offset-4\">

\t\t\t\t\t\t<div class=\"columns\">
\t\t\t\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t\t\t\t<img id=\"image-new-ad\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"image produit\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<div class=\"column is-half \">
\t\t\t\t\t\t<div class=\"container-is-fluid extended\">
\t\t\t\t\t\t\t<p class=\"title has-text-centered\">Sélectionner un réparateur</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"is-scrollable\">
\t\t\t\t\t\t\t";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 19));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 39
            echo "\t\t\t\t\t\t\t<a class=\"repairer is-fullwidth\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_seller_with_repair2");
            echo "\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "

\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-mobile is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t<div class=\"container-is-fluid extended\">
\t\t\t\t\t\t\t<p class=\"title has-text-centered\">Proposer une réparation</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"columns is-margin-top\">
\t\t\t\t\t\t\t<div class=\"column is-6 is-offset-3\">
\t\t\t\t\t\t\t\t<div class=\"columns has-text-centered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t\t\t\t\t<div class=\"field\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"input\" type=\"text\" placeholder=\"Votre prix\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t\t\t\t\t<button class=\"button is-outlined is-rounded is-info\">Soumettre</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">49.99€</span>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">
\t\t\t\t\t\t\tproduit</span>
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">16.50€</span>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">
\t\t\t\t\t\t\tréparation</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<p class=\"title text-footer\">66.49€</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"";
        // line 238
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact_seller_with_repair");
        echo "\">
\t\t\t\t\t\t<span>Contacter le réparateur et le vendeur</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t\t";
        // line 292
        echo "
\t</div>


</div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "select_repairer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  351 => 292,  339 => 238,  161 => 62,  131 => 39,  127 => 38,  107 => 21,  95 => 11,  85 => 10,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Sélection du réparateur</p>
</div>

{% endblock %}

{% block body %}

<div class=\"container\">
\t<div class=\"card\">
\t\t<div class=\"card-content\">
\t\t\t<div class=\"content\">
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<div class=\"column is-4 is-offset-4\">

\t\t\t\t\t\t<div class=\"columns\">
\t\t\t\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t\t\t\t<img id=\"image-new-ad\" src=\"{{ asset('img/ecran-casse.jpg') }}\" alt=\"image produit\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<div class=\"column is-half \">
\t\t\t\t\t\t<div class=\"container-is-fluid extended\">
\t\t\t\t\t\t\t<p class=\"title has-text-centered\">Sélectionner un réparateur</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"is-scrollable\">
\t\t\t\t\t\t\t{% for i in 0..19 %}
\t\t\t\t\t\t\t<a class=\"repairer is-fullwidth\" href=\"{{ path(\"contact_seller_with_repair2\") }}\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<hr>
\t\t\t\t\t\t\t{% endfor %}


\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-mobile is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"repairer\">
\t\t\t\t\t\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"subtitle\">Jacques Boulon</span>
\t\t\t\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t\t\t\t<span>
\t\t\t\t\t\t\t\t\t\t\t\t33600 PESSAC</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rate\">
\t\t\t\t\t\t\t\t\t\t\t<i class=\"star\">
\t\t\t\t\t\t\t\t\t\t\t\t4.3</i>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-3 has-text-centered\">
\t\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">15.00€</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t<div class=\"container-is-fluid extended\">
\t\t\t\t\t\t\t<p class=\"title has-text-centered\">Proposer une réparation</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"columns is-margin-top\">
\t\t\t\t\t\t\t<div class=\"column is-6 is-offset-3\">
\t\t\t\t\t\t\t\t<div class=\"columns has-text-centered\">
\t\t\t\t\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t\t\t\t\t<div class=\"field\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t\t\t\t\t\t<input class=\"input\" type=\"text\" placeholder=\"Votre prix\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"column is-half\">
\t\t\t\t\t\t\t\t\t\t<button class=\"button is-outlined is-rounded is-info\">Soumettre</button>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">49.99€</span>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">
\t\t\t\t\t\t\tproduit</span>
\t\t\t\t\t</p>
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">16.50€</span>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">
\t\t\t\t\t\t\tréparation</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<p class=\"title text-footer\">66.49€</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"{{ path(\"contact_seller_with_repair\") }}\">
\t\t\t\t\t\t<span>Contacter le réparateur et le vendeur</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t\t{# <footer class=\"contact-footer\">
\t\t\t<div class=\"columns is-marginless is-vcentered is-variable is-hidden-mobile\">
\t\t\t\t<div class=\"column is-4\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">49.99€</span>
\t\t\t\t\t\t<span class=\"text-footer\">
\t\t\t\t\t\t\tproduit</span>
\t\t\t\t\t</p>
\t\t\t\t\t<p>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">16.50€</span>
\t\t\t\t\t\t<span class=\"text-footer\">
\t\t\t\t\t\t\tRéparation</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column has-text-centered\">
\t\t\t\t\t<p class=\"title text-footer\">66.49€</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column has-text-right\">
\t\t\t\t\t<button class=\"button is-outlined is-rounded is-primary\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</button>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"columns is-marginless is-vcentered is-variable is-mobile is-hidden-tablet is-hidden-desktop\">
\t\t\t\t<div class=\"column is-2\">
\t\t\t\t\t<img src=\"{{ asset('img/ecran-casse-petit.png') }}\" alt=\"image du téléphone\" />
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t<p class=\"has-text-white\">Iphone 4</p>
\t\t\t\t\t<p class=\"has-text-grey\">Ecran cassé</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4\">
\t\t\t\t\t<button class=\"button is-outlined is-rounded is-primary\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer> #}

\t</div>


</div>


{% endblock %}", "select_repairer.html.twig", "/home/amaurel/ufix/Ufix/templates/select_repairer.html.twig");
    }
}
