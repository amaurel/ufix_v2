<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ad.html.twig */
class __TwigTemplate_51afdd9e490d2e70b4ee1d57211e05fa64d700f150971b6ab6470c15661aa309 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ad.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ad.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "ad.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 4
        echo "\t";
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"is-size-3 has-text-centered\">Anonce</p>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "
\t<div class=\"container\">

\t\t<div class=\"card\">
\t\t\t<header class=\"card-header\">
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<h2 class=\"column ad\">Iphone 5</h2>
\t\t\t\t\t<i class=\"far fa-user is-size-6\"></i>
\t\t\t\t</div>
\t\t\t\t<h3>Hors service</h3>

\t\t\t\t<img src=\"#\" alt=\"Image Iphone 5\">
\t\t\t</header>

\t\t\t<div class=\"card-content has-text-centered\">
\t\t\t\t<div class=\"content\">
\t\t\t\t\t<div class=\"columns\">
\t\t\t\t\t\t<div class=\"column\">
\t\t\t\t\t\t\t<h2 class=\"is-uppercase\">Détails Techniques</h2>
\t\t\t\t\t\t\t<table class=\"has-text-centered\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th>marque</th>
\t\t\t\t\t\t\t\t\t\t<th>modèle</th>
\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Apple</td>
\t\t\t\t\t\t\t\t\t\t<td>Iphone 5</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th>couleur</th>
\t\t\t\t\t\t\t\t\t\t<th>capacité</th>
\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Blanc</td>
\t\t\t\t\t\t\t\t\t\t<td>64 Go</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"column\">
\t\t\t\t\t\t\t<h2 class=\"is-uppercase\">Description</h2>
\t\t\t\t\t\t\t<p>Iphone 5 avec un problème matériel, le téléphone s'allume
\t\t\t\t\t\t\t\t                            et se charge mais reste bloqué sur l'écran de recherche du serveur d'activation d'Apple
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"column\">
\t\t\t\t\t\t\t<h2 class=\"is-uppercase\">Vendeur</h2>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<footer class=\"card-footer\">

\t\t\t\t<span class=\"fa-stack fa-2x\">
\t\t\t\t\t<i class=\"fas fa-circle fa-stack-1x fa\"></i>

\t\t\t\t\t<i class=\"far fa-user fa-stack-1x fa-inverse\"></i>
\t\t\t\t</span>


\t\t\t\t<button class=\"button is-outlined is-rounded is-custom-button-5\">
\t\t\t\t\tRéparer >
\t\t\t\t</button>
\t\t\t</footer>
\t\t</div>


\t</div>
</div></div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 11,  85 => 10,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block navbar %}
\t{{ parent() }}
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"is-size-3 has-text-centered\">Anonce</p>
\t</div>
{% endblock %}

{% block body %}

\t<div class=\"container\">

\t\t<div class=\"card\">
\t\t\t<header class=\"card-header\">
\t\t\t\t<div class=\"columns\">
\t\t\t\t\t<h2 class=\"column ad\">Iphone 5</h2>
\t\t\t\t\t<i class=\"far fa-user is-size-6\"></i>
\t\t\t\t</div>
\t\t\t\t<h3>Hors service</h3>

\t\t\t\t<img src=\"#\" alt=\"Image Iphone 5\">
\t\t\t</header>

\t\t\t<div class=\"card-content has-text-centered\">
\t\t\t\t<div class=\"content\">
\t\t\t\t\t<div class=\"columns\">
\t\t\t\t\t\t<div class=\"column\">
\t\t\t\t\t\t\t<h2 class=\"is-uppercase\">Détails Techniques</h2>
\t\t\t\t\t\t\t<table class=\"has-text-centered\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th>marque</th>
\t\t\t\t\t\t\t\t\t\t<th>modèle</th>
\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Apple</td>
\t\t\t\t\t\t\t\t\t\t<td>Iphone 5</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th>couleur</th>
\t\t\t\t\t\t\t\t\t\t<th>capacité</th>
\t\t\t\t\t\t\t\t\t</tr>

\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>Blanc</td>
\t\t\t\t\t\t\t\t\t\t<td>64 Go</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"column\">
\t\t\t\t\t\t\t<h2 class=\"is-uppercase\">Description</h2>
\t\t\t\t\t\t\t<p>Iphone 5 avec un problème matériel, le téléphone s'allume
\t\t\t\t\t\t\t\t                            et se charge mais reste bloqué sur l'écran de recherche du serveur d'activation d'Apple
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"column\">
\t\t\t\t\t\t\t<h2 class=\"is-uppercase\">Vendeur</h2>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<footer class=\"card-footer\">

\t\t\t\t<span class=\"fa-stack fa-2x\">
\t\t\t\t\t<i class=\"fas fa-circle fa-stack-1x fa\"></i>

\t\t\t\t\t<i class=\"far fa-user fa-stack-1x fa-inverse\"></i>
\t\t\t\t</span>


\t\t\t\t<button class=\"button is-outlined is-rounded is-custom-button-5\">
\t\t\t\t\tRéparer >
\t\t\t\t</button>
\t\t\t</footer>
\t\t</div>


\t</div>
</div></div>{% endblock %}
", "ad.html.twig", "/home/amaurel/ufix/Ufix/templates/ad.html.twig");
    }
}
