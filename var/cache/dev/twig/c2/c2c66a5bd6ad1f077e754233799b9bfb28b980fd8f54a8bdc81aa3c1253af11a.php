<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact.html.twig */
class __TwigTemplate_01a27a816897f6c161eb7219bf1fe11a4ad6cb91974ee1981489f955de487295 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contact.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tContacter
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 8
        echo "\t";
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"title has-text-centered\">Contacter</p>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "\t<div class=\"container has-text-centered\">
\t\t<div class=\"columns is-8 is-variable is-vcentered\">

\t\t\t<div class=\"column is-seven has-text-left\">
\t\t\t\t<h1 class=\"title is-1\">Contactez-nous</h1>
\t\t\t\t<p class=\"is-size-4\">
\t\t\t\t\tNous serons ravis de répondre à vos questions et à vos remarques.
\t\t\t\t</p>
\t\t\t\t<div class=\"social-media\">
\t\t\t\t\t<a href=\"https://www.facebook.com/uFixFR/\" class=\"icon\">
\t\t\t\t\t\t<i class=\"fab fa-facebook-square fa-3x\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"https://twitter.com/UFixFR\" class=\"icon\">
\t\t\t\t\t\t<i class=\"fab fa-twitter-square fa-3x\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"column is-5 has-text-left\">
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Nom</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<input class=\"input is-medium\" type=\"text\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Email</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<input class=\"input is-medium\" type=\"text\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Message</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<textarea class=\"textarea is-medium\"></textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"control\">
\t\t\t\t\t<button type=\"submit\" class=\"button is-fullwidth has-text-weight-medium is-medium is-primary\">Envoyer</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 15,  107 => 14,  91 => 8,  81 => 7,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tContacter
{% endblock %}

{% block navbar %}
\t{{ parent() }}
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"title has-text-centered\">Contacter</p>
\t</div>
{% endblock %}

{% block body %}
\t<div class=\"container has-text-centered\">
\t\t<div class=\"columns is-8 is-variable is-vcentered\">

\t\t\t<div class=\"column is-seven has-text-left\">
\t\t\t\t<h1 class=\"title is-1\">Contactez-nous</h1>
\t\t\t\t<p class=\"is-size-4\">
\t\t\t\t\tNous serons ravis de répondre à vos questions et à vos remarques.
\t\t\t\t</p>
\t\t\t\t<div class=\"social-media\">
\t\t\t\t\t<a href=\"https://www.facebook.com/uFixFR/\" class=\"icon\">
\t\t\t\t\t\t<i class=\"fab fa-facebook-square fa-3x\"></i>
\t\t\t\t\t</a>
\t\t\t\t\t<a href=\"https://twitter.com/UFixFR\" class=\"icon\">
\t\t\t\t\t\t<i class=\"fab fa-twitter-square fa-3x\"></i>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"column is-5 has-text-left\">
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Nom</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<input class=\"input is-medium\" type=\"text\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Email</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<input class=\"input is-medium\" type=\"text\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"field\">
\t\t\t\t\t<label class=\"label\">Message</label>
\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t<textarea class=\"textarea is-medium\"></textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"control\">
\t\t\t\t\t<button type=\"submit\" class=\"button is-fullwidth has-text-weight-medium is-medium is-primary\">Envoyer</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
{% endblock %}
", "contact.html.twig", "/home/amaurel/ufix/Ufix/templates/contact.html.twig");
    }
}
