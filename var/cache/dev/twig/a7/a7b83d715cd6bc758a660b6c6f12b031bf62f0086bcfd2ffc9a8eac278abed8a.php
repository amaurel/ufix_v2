<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* messaging.html.twig */
class __TwigTemplate_108ada9398011c15be9e96959d39365671f717d76587505e3ab179b6291744bf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'aside' => [$this, 'block_aside'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "messaging.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "messaging.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "messaging.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Messagerie
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Messagerie</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_aside($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "aside"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "aside"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "
<div class=\"container\">
  <div class=\"card\">
    <div class=\"card-content\">
      <div class=\"content\">
        <div class=\"columns\">
          <div class=\"column is-2 is-fullheight contacts is-paddingless\">
          
            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 6));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 22
            echo "              <div class=\"contact\">
                <div class=\"columns\">
                  <div class=\"column\">
                    <div class=\"columns is-vcentered\">
                      <div class=\"column\">
                        <p>
                          <span class=\"title icon-before-msg\">Jacques Pommier</span>
                          <br/>
                          <span class=\"subtitle\">Vendeur</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr class=\"b-1b\"/>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "            
          </div>
          <div class=\"column is-10 is-paddingless chat\">
            <div class=\"columns has-text-centered\">
              <div class=\"column is-4 is-offset-4\">
                <p class=\"title\">Jacques Pommier</p>
                <p class=\"subtitle\">Vendeur</p>
              </div>
            </div>
            <!-- end chat-header -->

            <div class=\"chat-history\">
              <ul  class=\"margin-form\" id=\"messages\">
                <li class=\"clearfix\">
                  <div class=\"message-data has-text-right\">
                    <span class=\"message-data-time\">10:10 AM, Today</span>
                    &nbsp; &nbsp;
                    <span class=\"message-data-name\">Olia</span>
                  </div>
                  <div class=\"message other-message is-pulled-right\">
                      Quisque sagittis blandit nunc, vitae ultricies odio porttitor quis. Vivamus mattis urna orci.
                  </div>
                </li>

                <li>
                  <div class=\"message-data\">
                    <span class=\"message-data-name\"> Vincent</span>
                    <span class=\"message-data-time\">10:12 AM, Today</span>
                  </div>
                  <div class=\"message my-message\">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut varius, magna id imperdiet fermentum, nunc 
                      ipsum faucibus libero, non dictum leo felis ut quam. Ut vitae dui laoreet, convallis risus vulputate,
                  </div>
                </li>

                <li class=\"clearfix\">
                  <div class=\"message-data has-text-right\">
                    <span class=\"message-data-time\">10:14 AM, Today</span>
                    &nbsp; &nbsp;
                    <span class=\"message-data-name\">Olia</span>
                  </div>
                  <div class=\"message other-message is-pulled-right\">
                    Vivamus rhoncus cursus ligula vitae tincidunt. Nullam urna ex, congue ac semper nec, varius ac magna. Nulla facilisi. Sed ac leo eget purus blandit sodales nec sed ipsum. Maecenas dignissim ipsum lorem, nec aliquet risus semper eget. Vestibulum sit amet lectus et elit dictum porta. Curabitur ac laoreet neque. Pellentesque lobortis urna id eros 
                  </div>
                </li>

                <li>
                  <div class=\"message-data\">
                    <span class=\"message-data-name\"> Vincent</span>
                    <span class=\"message-data-time\">10:20 AM, Today</span>
                  </div>
                  <div class=\"message my-message\">
                      Sed eu ante euismod, sollicitudin libero eu, pretium tortor. Vivamus sit amet velit diam. Nam egestas tincidunt pharetra. Cras efficitur diam scelerisque felis congue, nec 
                  </div>
                </li>
              </ul>
            </div>
            <!-- end chat-history -->

            <div class=\"chat-message clearfix\">
              <textarea
                name=\"message-to-send\"
                id=\"message-to-send\"
                placeholder=\"Type your message\"
                rows=\"3\"
              ></textarea>

              <button
                class=\"button is-outlined is-rounded is-primary is-pulled-right\"
                id=\"send-message\"
                onclick=\"sendMessage()\"
              >
                <span>Envoyer</span>
                <span class=\"icon\">
                  <i class=\"fas fa-angle-right\"></i>
                </span>
              </button>
            </div>

            <!-- end chat-message -->

            <!-- end chat -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "messaging.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 39,  150 => 22,  146 => 21,  136 => 13,  109 => 12,  94 => 8,  84 => 7,  71 => 4,  61 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} 

{% block title %}
\t{{ parent() }} - Messagerie
{% endblock %}

{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Messagerie</p>
</div>
{% endblock %} {% block aside %}{% endblock %} {% block body %}

<div class=\"container\">
  <div class=\"card\">
    <div class=\"card-content\">
      <div class=\"content\">
        <div class=\"columns\">
          <div class=\"column is-2 is-fullheight contacts is-paddingless\">
          
            {% for i in 0..6 %}
              <div class=\"contact\">
                <div class=\"columns\">
                  <div class=\"column\">
                    <div class=\"columns is-vcentered\">
                      <div class=\"column\">
                        <p>
                          <span class=\"title icon-before-msg\">Jacques Pommier</span>
                          <br/>
                          <span class=\"subtitle\">Vendeur</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr class=\"b-1b\"/>
            {% endfor %}
            
          </div>
          <div class=\"column is-10 is-paddingless chat\">
            <div class=\"columns has-text-centered\">
              <div class=\"column is-4 is-offset-4\">
                <p class=\"title\">Jacques Pommier</p>
                <p class=\"subtitle\">Vendeur</p>
              </div>
            </div>
            <!-- end chat-header -->

            <div class=\"chat-history\">
              <ul  class=\"margin-form\" id=\"messages\">
                <li class=\"clearfix\">
                  <div class=\"message-data has-text-right\">
                    <span class=\"message-data-time\">10:10 AM, Today</span>
                    &nbsp; &nbsp;
                    <span class=\"message-data-name\">Olia</span>
                  </div>
                  <div class=\"message other-message is-pulled-right\">
                      Quisque sagittis blandit nunc, vitae ultricies odio porttitor quis. Vivamus mattis urna orci.
                  </div>
                </li>

                <li>
                  <div class=\"message-data\">
                    <span class=\"message-data-name\"> Vincent</span>
                    <span class=\"message-data-time\">10:12 AM, Today</span>
                  </div>
                  <div class=\"message my-message\">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut varius, magna id imperdiet fermentum, nunc 
                      ipsum faucibus libero, non dictum leo felis ut quam. Ut vitae dui laoreet, convallis risus vulputate,
                  </div>
                </li>

                <li class=\"clearfix\">
                  <div class=\"message-data has-text-right\">
                    <span class=\"message-data-time\">10:14 AM, Today</span>
                    &nbsp; &nbsp;
                    <span class=\"message-data-name\">Olia</span>
                  </div>
                  <div class=\"message other-message is-pulled-right\">
                    Vivamus rhoncus cursus ligula vitae tincidunt. Nullam urna ex, congue ac semper nec, varius ac magna. Nulla facilisi. Sed ac leo eget purus blandit sodales nec sed ipsum. Maecenas dignissim ipsum lorem, nec aliquet risus semper eget. Vestibulum sit amet lectus et elit dictum porta. Curabitur ac laoreet neque. Pellentesque lobortis urna id eros 
                  </div>
                </li>

                <li>
                  <div class=\"message-data\">
                    <span class=\"message-data-name\"> Vincent</span>
                    <span class=\"message-data-time\">10:20 AM, Today</span>
                  </div>
                  <div class=\"message my-message\">
                      Sed eu ante euismod, sollicitudin libero eu, pretium tortor. Vivamus sit amet velit diam. Nam egestas tincidunt pharetra. Cras efficitur diam scelerisque felis congue, nec 
                  </div>
                </li>
              </ul>
            </div>
            <!-- end chat-history -->

            <div class=\"chat-message clearfix\">
              <textarea
                name=\"message-to-send\"
                id=\"message-to-send\"
                placeholder=\"Type your message\"
                rows=\"3\"
              ></textarea>

              <button
                class=\"button is-outlined is-rounded is-primary is-pulled-right\"
                id=\"send-message\"
                onclick=\"sendMessage()\"
              >
                <span>Envoyer</span>
                <span class=\"icon\">
                  <i class=\"fas fa-angle-right\"></i>
                </span>
              </button>
            </div>

            <!-- end chat-message -->

            <!-- end chat -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{% endblock %}
", "messaging.html.twig", "/home/amaurel/ufix/Ufix/templates/messaging.html.twig");
    }
}
