<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contactToRepair.html.twig */
class __TwigTemplate_019643856f6a0b3d0aca7799056df368c6d5d5a5f271b8fe47f25777c62bf030 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contactToRepair.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contactToRepair.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contactToRepair.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 3
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Contacter le vendeur</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
\t<button class=\"delete\"></button>
\t<p>Message envoyé !</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "<div class=\"container\">
\t<div class=\"card\">
\t\t<div class=\"card-header p-3t\">
\t\t\t<div class=\"has-text-centered\">
\t\t\t\t<p class=\" title is-size-4\">Pascal MOLIPOU</p>
\t\t\t\t<p class=\"title is-size-5 has-text-grey\">PESSAC - 33600</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"card-content has-text-centered\">
\t\t\t<div class=\"content\">
\t\t\t\t<form action=\"\">
\t\t\t\t\t<div class=\"field margin-contact\">
\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t<textarea class=\"textarea min-height-textarea\" required>
Bonjour, 
              
Je suis en mesure de réparer votre appareil.
Quand et où pouvons-nous nous donner rendez-vous ?

Cordialement,
Léo POMA
\t\t\t\t\t\t\t\t</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>

\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Iphone 5</span>
\t\t\t\t\t\t<br/>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Hors service</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("messaging");
        echo "\" onclick=\"isSend()\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>\t
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t\t";
        // line 97
        echo "
\t</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contactToRepair.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 97,  138 => 51,  98 => 13,  88 => 12,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Contacter le vendeur</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
\t<button class=\"delete\"></button>
\t<p>Message envoyé !</p>
</div>
{% endblock %}
{% block body %}
<div class=\"container\">
\t<div class=\"card\">
\t\t<div class=\"card-header p-3t\">
\t\t\t<div class=\"has-text-centered\">
\t\t\t\t<p class=\" title is-size-4\">Pascal MOLIPOU</p>
\t\t\t\t<p class=\"title is-size-5 has-text-grey\">PESSAC - 33600</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"card-content has-text-centered\">
\t\t\t<div class=\"content\">
\t\t\t\t<form action=\"\">
\t\t\t\t\t<div class=\"field margin-contact\">
\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t<textarea class=\"textarea min-height-textarea\" required>
Bonjour, 
              
Je suis en mesure de réparer votre appareil.
Quand et où pouvons-nous nous donner rendez-vous ?

Cordialement,
Léo POMA
\t\t\t\t\t\t\t\t</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>

\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Iphone 5</span>
\t\t\t\t\t\t<br/>
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Hors service</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"{{ path(\"messaging\") }}\" onclick=\"isSend()\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>\t
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>
\t\t{# <footer class=\"contact-footer\">
\t\t\t<div class=\"columns is-marginless is-vcentered is-variable is-hidden-mobile\">
\t\t\t\t<div class=\"column is-1\">
\t\t\t\t\t<img src=\"{{ asset('img/iphone5-petit.png') }}\" alt=\"image du téléphone\" />
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-9\">
\t\t\t\t\t<p class=\"has-text-white\">Iphone 5</p>
\t\t\t\t\t<p class=\"has-text-grey\">Hors service</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"{{ path(\"messaging\") }}\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"columns is-marginless is-vcentered is-variable is-mobile is-hidden-tablet is-hidden-desktop\">
\t\t\t\t<div class=\"column is-2\">
\t\t\t\t\t<img src=\"{{ asset('img/iphone5-petit.png') }}\" alt=\"image du téléphone\" />
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6\">
\t\t\t\t\t<p class=\"has-text-white\">Iphone 5</p>
\t\t\t\t\t<p class=\"has-text-grey\">Hors service</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-4\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"{{ path(\"messaging\") }}\" onclick=\"isSend()\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer> #}

\t</div>
</div>
{% endblock %}", "contactToRepair.html.twig", "/home/amaurel/ufix/Ufix/templates/contactToRepair.html.twig");
    }
}
