<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home_connected.html.twig */
class __TwigTemplate_2b5f2a6fd5612ba18c969f68ba70ac54a4443277497959e9b2c1f33025bf5796 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home_connected.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home_connected.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home_connected.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Annonces
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Annonces</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "<div class=\"container is-fluid-without-padding\">
\t<!-- Top part-->
\t<div class=\"columns is-centered p-3t\">
\t\t<div class=\"column is-4\" id=\"search_bar\">
\t\t\t<article class=\"panel\">
\t\t\t\t<p class=\"panel-tabs\">
\t\t\t\t\t<a class=\"is-active\">Tous</a>
\t\t\t\t\t<a>Mobile</a>
\t\t\t\t\t<a>Ordinateur</a>
\t\t\t\t\t<a>Tablette</a>
\t\t\t\t</p>
\t\t\t\t<div class=\"panel-block\">
\t\t\t\t\t<p class=\"control has-icons-left\">
\t\t\t\t\t\t<input class=\"input is-dark\" type=\"text\" placeholder=\"Search\" />
\t\t\t\t\t\t<span class=\"icon is-left\">
\t\t\t\t\t\t\t<i class=\"fas fa-search\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</article>
\t\t</div>
\t</div>
\t<!-- Grid of items-->
\t<div class=\"columns\">
\t\t<div class=\"column is-10 is-offset-1\">
\t\t\t<div class=\"columns is-mobile is-multiline products\">
\t\t\t\t";
        // line 42
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_repair");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone5.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 61
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 80
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 99
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 118
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_repair");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone5.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 137
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 156
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 175
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 194
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_repair");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 198
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone5.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 213
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 232
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 251
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 270
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_repair");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 274
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone5.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 289
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 293
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 308
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 312
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 327
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 331
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 346
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_repair");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 350
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone5.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 365
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 369
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 384
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 388
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t";
        // line 403
        echo "\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 407
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse.jpg"), "html", null, true);
        echo "\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home_connected.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  629 => 407,  621 => 403,  604 => 388,  596 => 384,  579 => 369,  571 => 365,  554 => 350,  546 => 346,  529 => 331,  521 => 327,  504 => 312,  496 => 308,  479 => 293,  471 => 289,  454 => 274,  446 => 270,  429 => 255,  421 => 251,  404 => 236,  396 => 232,  379 => 217,  371 => 213,  354 => 198,  346 => 194,  329 => 179,  321 => 175,  304 => 160,  296 => 156,  279 => 141,  271 => 137,  254 => 122,  246 => 118,  229 => 103,  221 => 99,  204 => 84,  196 => 80,  179 => 65,  171 => 61,  154 => 46,  146 => 42,  118 => 15,  108 => 14,  93 => 8,  83 => 7,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\t{{ parent() }} - Annonces
{% endblock %}

{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Annonces</p>
</div>
{% endblock %}

{% block body %}
<div class=\"container is-fluid-without-padding\">
\t<!-- Top part-->
\t<div class=\"columns is-centered p-3t\">
\t\t<div class=\"column is-4\" id=\"search_bar\">
\t\t\t<article class=\"panel\">
\t\t\t\t<p class=\"panel-tabs\">
\t\t\t\t\t<a class=\"is-active\">Tous</a>
\t\t\t\t\t<a>Mobile</a>
\t\t\t\t\t<a>Ordinateur</a>
\t\t\t\t\t<a>Tablette</a>
\t\t\t\t</p>
\t\t\t\t<div class=\"panel-block\">
\t\t\t\t\t<p class=\"control has-icons-left\">
\t\t\t\t\t\t<input class=\"input is-dark\" type=\"text\" placeholder=\"Search\" />
\t\t\t\t\t\t<span class=\"icon is-left\">
\t\t\t\t\t\t\t<i class=\"fas fa-search\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</article>
\t\t</div>
\t</div>
\t<!-- Grid of items-->
\t<div class=\"columns\">
\t\t<div class=\"column is-10 is-offset-1\">
\t\t\t<div class=\"columns is-mobile is-multiline products\">
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_repair') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone5.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone4.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone6.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/ecran-casse.jpg') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_repair') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone5.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone4.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone6.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/ecran-casse.jpg') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_repair') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone5.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone4.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone6.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/ecran-casse.jpg') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_repair') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone5.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone4.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone6.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/ecran-casse.jpg') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_repair') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone5.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title to-repare\">Iphone 5</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">-- €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone4.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Hors service</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">45.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/iphone6.png') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 6</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Lightning foutue</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">98.00 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t\t{# COLUMN #}
\t\t\t\t<a class=\"column is-2-desktop is-3-tablet is-6-mobile\" href=\"{{ path('to_sell') }}\">
\t\t\t\t\t<div class=\"card all-ads\">
\t\t\t\t\t\t<div class=\"card-image\">
\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t<img src=\"{{ asset('img/ecran-casse.jpg') }}\" alt=\"Iphone 5\">
\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"card-content\">
\t\t\t\t\t\t\t<div class=\"media\">
\t\t\t\t\t\t\t\t<div class=\"media-content has-text-centered\">
\t\t\t\t\t\t\t\t\t<p class=\"title\">Iphone 4</p>
\t\t\t\t\t\t\t\t\t<p class=\"subtitle\">Ecran cassé</p>
\t\t\t\t\t\t\t\t\t<p class=\"susbtitle\">49.99 €</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
{% endblock %}", "home_connected.html.twig", "/home/amaurel/dawin_plans/ufix/Ufix/templates/home_connected.html.twig");
    }
}
