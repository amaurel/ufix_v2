<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contactSellerRepair2.html.twig */
class __TwigTemplate_4b280cc0f6861a67a5b65fb068769860b01d40020e0b5feb76892a64133162c0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contactSellerRepair2.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contactSellerRepair2.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contactSellerRepair2.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 3
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Contacter le réparateur</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
\t<button class=\"delete\"></button>
\t<p>Message envoyé !</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "<div class=\"container\">
\t<div class=\"card\">
\t\t<div class=\"card-header p-3t\">
\t\t\t<div class=\"has-text-centered\">
\t\t\t\t<p class=\" title is-size-4\">Marie TYPOU</p>
\t\t\t\t<p class=\"title is-size-5 has-text-grey\">GRADIGNAN - 33170</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"card-content has-text-centered\">
\t\t\t<div class=\"content\">
\t\t\t\t<form action=\"\">
\t\t\t\t\t<div class=\"field margin-contact\">
\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t<textarea class=\"textarea min-height-textarea\" required>
Bonjour, 
              
Je suis intéressé par votre offre de réparation, j'ai pris contact avec le
vendeur du produit et si sa réponse est positive je le ferai réparer par vos
soir.

Cordialement,
Léo POMA
\t\t\t\t\t\t\t\t</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>

\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Iphone 4</span>
\t\t\t\t\t\t<br />
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Ecran cassé</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"";
        // line 52
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("messaging");
        echo "\"
\t\t\t\t\t\tonclick=\"isSend()\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>

\t</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contactSellerRepair2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 52,  98 => 13,  88 => 12,  69 => 3,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
\t<p class=\"title has-text-centered\">Contacter le réparateur</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
\t<button class=\"delete\"></button>
\t<p>Message envoyé !</p>
</div>
{% endblock %}
{% block body %}
<div class=\"container\">
\t<div class=\"card\">
\t\t<div class=\"card-header p-3t\">
\t\t\t<div class=\"has-text-centered\">
\t\t\t\t<p class=\" title is-size-4\">Marie TYPOU</p>
\t\t\t\t<p class=\"title is-size-5 has-text-grey\">GRADIGNAN - 33170</p>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"card-content has-text-centered\">
\t\t\t<div class=\"content\">
\t\t\t\t<form action=\"\">
\t\t\t\t\t<div class=\"field margin-contact\">
\t\t\t\t\t\t<div class=\"control\">
\t\t\t\t\t\t\t<textarea class=\"textarea min-height-textarea\" required>
Bonjour, 
              
Je suis intéressé par votre offre de réparation, j'ai pris contact avec le
vendeur du produit et si sa réponse est positive je le ferai réparer par vos
soir.

Cordialement,
Léo POMA
\t\t\t\t\t\t\t\t</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>

\t\t<footer class=\"custom-card-footer\">
\t\t\t<div class=\"columns is-vcentered\">
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<p class=\"subtitle text-footer\">
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Iphone 4</span>
\t\t\t\t\t\t<br />
\t\t\t\t\t\t<span class=\"subtitle text-footer\">Ecran cassé</span>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"column is-6 has-text-centered\">
\t\t\t\t\t<a class=\"button is-outlined is-rounded is-info\" href=\"{{ path(\"messaging\") }}\"
\t\t\t\t\t\tonclick=\"isSend()\">
\t\t\t\t\t\t<span>Envoyer</span>
\t\t\t\t\t\t<span class=\"icon\">
\t\t\t\t\t\t\t<i class=\"fas fa-angle-right\"></i>
\t\t\t\t\t\t</span>
\t\t\t\t\t</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</footer>

\t</div>
</div>
{% endblock %}", "contactSellerRepair2.html.twig", "/home/amaurel/ufix/Ufix/templates/contactSellerRepair2.html.twig");
    }
}
