<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_c1dbeab3e984a89e00cadea6c500a613a923317758a6d831464a51535689a1e3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>
      ";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        // line 9
        echo "    </title>
    ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 18
        echo "  </head>
  <body>
    <section class=\"hero\">
      <!-- Page head -->
      <div class=\"hero-head\">
        <!-- Navbar -->
        ";
        // line 24
        $this->displayBlock('navbar', $context, $blocks);
        // line 78
        echo "      </div>
    </section>
    
    <section class=\"hero is-fullheight-with-navbar\">
      <div class=\"hero-body verticaly-top\">
        ";
        // line 83
        $this->displayBlock('body', $context, $blocks);
        // line 84
        echo "      </div>
      <!-- Footer -->
      <div class=\"hero-footer\">
        ";
        // line 87
        $this->displayBlock('footer', $context, $blocks);
        // line 122
        echo "      </div>
    </section>

    ";
        // line 125
        $this->displayBlock('javascripts', $context, $blocks);
        // line 136
        echo "  </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "UFiX ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/app.css"), "html", null, true);
        echo "\" />
    <link
      rel=\"icon\"
      type=\"image/png\"
      href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon-onglet.png"), "html", null, true);
        echo "\"
    />
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 24
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 25
        echo "        <nav class=\"navbar\" role=\"navigation\" aria-label=\"main navigation\">
          <div class=\"navbar-brand\">
            <a class=\"navbar-item\" href=\"";
        // line 27
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_page");
        echo "\">
              <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo_white.png"), "html", null, true);
        echo "\" alt=\"Logo UFix\"/>
            </a>
            <a
              role=\"button\"
              class=\"navbar-burger burger\"
              aria-label=\"menu\"
              aria-expanded=\"false\"
              data-target=\"navbarBasicExample\"
            >
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
            </a>
          </div>

          <div id=\"navbarBasicExample\" class=\"navbar-menu\">
            <div class=\"navbar-center is-size-6\">
              <a class=\"navbar-item\" href=\"";
        // line 45
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_ad");
        echo "\">
                Déposer une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_connected");
        echo "\">
                Rechercher une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 51
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_product");
        echo "\">
                Mes annonces
              </a>
            </div>
            <div class=\"navbar-center-mobile is-hidden-desktop\"></div>
            <div class=\"navbar-end\">
              <a class=\"navbar-item\" href=\"";
        // line 57
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_page");
        echo "\">
                <span class=\"icon is-medium\">
                  <i class=\"far fa-user is-size-6\"></i>
                </span>
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 62
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("messaging");
        echo "\">
                <span
                  class=\"icon is-small-desktop is-medium-mobile is-medium-tablet\"
                >
                  <i class=\"far fa-comment-alt is-size-6\"></i>
                </span>
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 69
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("ads_saved");
        echo "\">
                <span class=\"icon is-medium\">
                  <i class=\"far fa-heart is-size-6\"></i>
                </span>
              </a>
            </div>
          </div>
        </nav>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 83
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 87
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 88
        echo "        <div class=\"columns is-vcentered\">
          <div class=\"column\">
            <p
              class=\"has-text-left-desktop has-text-centered-mobile has-text-left-tablet\"
            >
              <a href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("legal_mentions");
        echo "\" class=\"is-size-7\">Mentions légales</a>
              |
              <a href=\"";
        // line 95
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("contact");
        echo "\" class=\"is-size-7\">Nous contater</a>
              |
              <a href=\"";
        // line 97
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cgu");
        echo "\" class=\"is-size-7\">CGU</a>
            </p>
          </div>
          <div class=\"column\">
            <p class=\"has-text-centered is-size-7\">@UFiX 2019</p>
          </div>
          <div class=\"column\">
            <p
              class=\"has-text-right-desktop has-text-centered-mobile is-size-7 has-text-right-tablet\"
            >
              Retrouvez-nous sur :
              <a href=\"https://twitter.com/UFixFR\" class=\"icon-reseaux\">
                <span class=\"icon is-medium\">
                  <i class=\"fab fa-twitter\"></i>
                </span>
              </a>
              <a href=\"https://www.facebook.com/uFixFR/\" class=\"icon-reseaux\">
                <span class=\"icon is-small\">
                  <i class=\"fab fa-facebook-f\"></i>
                </span>
              </a>
            </p>
          </div>
        </div>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 125
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 126
        echo "    <script
      type=\"text/javascript\"
      src=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js\"
    ></script>
    <script
      type=\"text/javascript\"
      src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js\"
    ></script>
    <script type=\"text/javascript\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  356 => 134,  346 => 126,  336 => 125,  301 => 97,  296 => 95,  291 => 93,  284 => 88,  274 => 87,  256 => 83,  237 => 69,  227 => 62,  219 => 57,  210 => 51,  204 => 48,  198 => 45,  178 => 28,  174 => 27,  170 => 25,  160 => 24,  147 => 15,  139 => 11,  129 => 10,  110 => 8,  98 => 136,  96 => 125,  91 => 122,  89 => 87,  84 => 84,  82 => 83,  75 => 78,  73 => 24,  65 => 18,  63 => 10,  60 => 9,  58 => 8,  49 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"fr\">
  <head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>
      {% block title %}UFiX {% endblock %}
    </title>
    {% block stylesheets %}
    <link rel=\"stylesheet\" href=\"{{ asset('build/css/app.css') }}\" />
    <link
      rel=\"icon\"
      type=\"image/png\"
      href=\"{{ asset('img/icon-onglet.png') }}\"
    />
    {% endblock %}
  </head>
  <body>
    <section class=\"hero\">
      <!-- Page head -->
      <div class=\"hero-head\">
        <!-- Navbar -->
        {% block navbar %}
        <nav class=\"navbar\" role=\"navigation\" aria-label=\"main navigation\">
          <div class=\"navbar-brand\">
            <a class=\"navbar-item\" href=\"{{ path('home_page') }}\">
              <img src=\"{{ asset('img/logo_white.png') }}\" alt=\"Logo UFix\"/>
            </a>
            <a
              role=\"button\"
              class=\"navbar-burger burger\"
              aria-label=\"menu\"
              aria-expanded=\"false\"
              data-target=\"navbarBasicExample\"
            >
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
            </a>
          </div>

          <div id=\"navbarBasicExample\" class=\"navbar-menu\">
            <div class=\"navbar-center is-size-6\">
              <a class=\"navbar-item\" href=\"{{ path('new_ad') }}\">
                Déposer une annonce
              </a>
              <a class=\"navbar-item\" href=\"{{ path('home_connected') }}\">
                Rechercher une annonce
              </a>
              <a class=\"navbar-item\" href=\"{{ path('edit_product') }}\">
                Mes annonces
              </a>
            </div>
            <div class=\"navbar-center-mobile is-hidden-desktop\"></div>
            <div class=\"navbar-end\">
              <a class=\"navbar-item\" href=\"{{ path('profil_page') }}\">
                <span class=\"icon is-medium\">
                  <i class=\"far fa-user is-size-6\"></i>
                </span>
              </a>
              <a class=\"navbar-item\" href=\"{{ path('messaging') }}\">
                <span
                  class=\"icon is-small-desktop is-medium-mobile is-medium-tablet\"
                >
                  <i class=\"far fa-comment-alt is-size-6\"></i>
                </span>
              </a>
              <a class=\"navbar-item\" href=\"{{ path('ads_saved') }}\">
                <span class=\"icon is-medium\">
                  <i class=\"far fa-heart is-size-6\"></i>
                </span>
              </a>
            </div>
          </div>
        </nav>
        {% endblock %}
      </div>
    </section>
    
    <section class=\"hero is-fullheight-with-navbar\">
      <div class=\"hero-body verticaly-top\">
        {% block body %}{% endblock %}
      </div>
      <!-- Footer -->
      <div class=\"hero-footer\">
        {% block footer %}
        <div class=\"columns is-vcentered\">
          <div class=\"column\">
            <p
              class=\"has-text-left-desktop has-text-centered-mobile has-text-left-tablet\"
            >
              <a href=\"{{ path('legal_mentions') }}\" class=\"is-size-7\">Mentions légales</a>
              |
              <a href=\"{{ path('contact') }}\" class=\"is-size-7\">Nous contater</a>
              |
              <a href=\"{{ path('cgu') }}\" class=\"is-size-7\">CGU</a>
            </p>
          </div>
          <div class=\"column\">
            <p class=\"has-text-centered is-size-7\">@UFiX 2019</p>
          </div>
          <div class=\"column\">
            <p
              class=\"has-text-right-desktop has-text-centered-mobile is-size-7 has-text-right-tablet\"
            >
              Retrouvez-nous sur :
              <a href=\"https://twitter.com/UFixFR\" class=\"icon-reseaux\">
                <span class=\"icon is-medium\">
                  <i class=\"fab fa-twitter\"></i>
                </span>
              </a>
              <a href=\"https://www.facebook.com/uFixFR/\" class=\"icon-reseaux\">
                <span class=\"icon is-small\">
                  <i class=\"fab fa-facebook-f\"></i>
                </span>
              </a>
            </p>
          </div>
        </div>
        {% endblock %}
      </div>
    </section>

    {% block javascripts %}
    <script
      type=\"text/javascript\"
      src=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js\"
    ></script>
    <script
      type=\"text/javascript\"
      src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js\"
    ></script>
    <script type=\"text/javascript\" src=\"{{ asset('js/app.js') }}\"></script>
    {% endblock %}
  </body>
</html>
", "base.html.twig", "/home/amaurel/ufix2/ufix_v2/templates/base.html.twig");
    }
}
