<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html copy.twig */
class __TwigTemplate_fa58e9da5581c6f4730a3db7f92032b2626b9a575c1e34f57bbf732e863bed69 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html copy.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html copy.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "  </head>
  <body>
    <section class=\"hero\">
      <div class=\"hero-head is-fixed-top\">
        ";
        // line 13
        $this->displayBlock('navbar', $context, $blocks);
        // line 67
        echo "      </div>
    </section>

    <section class=\"hero is-fullheight-with-navbar\">
      <div class=\"hero-body\">
        ";
        // line 72
        $this->displayBlock('body', $context, $blocks);
        // line 73
        echo "      </div>
      <div class=\"hero-footer\">
        ";
        // line 75
        $this->displayBlock('footer', $context, $blocks);
        // line 109
        echo "      </div>
    </section>

    ";
        // line 112
        $this->displayBlock('javascripts', $context, $blocks);
        // line 154
        echo "  </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/css/app.css"), "html", null, true);
        echo "\" />
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 14
        echo "        <nav class=\"navbar\" role=\"navigation\" aria-label=\"main navigation\">
          <div class=\"navbar-brand\">
            <a class=\"navbar-item\" href=\"#\">
              <img src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo_white.png"), "html", null, true);
        echo "\" />
            </a>

            <a
              role=\"button\"
              class=\"navbar-burger burger\"
              aria-label=\"menu\"
              aria-expanded=\"false\"
              data-target=\"navbarBasicExample\"
            >
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
            </a>
          </div>

          <div id=\"navbarBasicExample\" class=\"navbar-menu\">
            <div class=\"navbar-center is-size-6\">
              <a class=\"navbar-item\" href=\"";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_ad");
        echo "\">
                Déposer une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_ad");
        echo "\">
                Rechercher une annonce
              </a>
              <a class=\"navbar-item\" href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("new_ad");
        echo "\">
                Mes annonces
              </a>
            </div>
            <div class=\"navbar-center-mobile is-hidden-desktop\"></div>

            <div class=\"navbar-end\">
              <div class=\"navbar-item\">
                <a href=\"#\">
                  <span class=\"icon is-medium\">
                    <i class=\"far fa-user is-size-6\"></i>
                  </span>
                  <span
                    class=\"icon is-small-desktop is-medium-mobile is-medium-tablet\"
                  >
                    <i class=\"far fa-comment-alt is-size-6\"></i>
                  </span>
                  <span class=\"icon is-medium\">
                    <i class=\"far fa-heart is-size-6\"></i>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </nav>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 72
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 75
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 76
        echo "        <div class=\"columns is-vcentered\">
          <div class=\"column\">
            <p
              class=\"has-text-left-desktop has-text-centered-mobile has-text-left-tablet\"
            >
              <a href=\"#\" class=\"is-size-7\">Mentions légales</a>
              |
              <a href=\"#\" class=\"is-size-7\">Protection des données</a>
              |
              <a href=\"#\" class=\"is-size-7\">CGV</a>
            </p>
          </div>
          <div class=\"column\">
            <p class=\"has-text-centered is-size-7\">@UFiX 2019</p>
          </div>
          <div class=\"column\">
            <p
              class=\"has-text-right-desktop has-text-centered-mobile is-size-7 has-text-right-tablet\"
            >
              Retrouvez-nous sur :
              <span class=\"icon is-medium\">
                <i class=\"fab fa-twitter\"></i>
              </span>
              <span class=\"icon is-small\">
                <i class=\"fab fa-facebook-f\"></i>
              </span>
              <span class=\"icon is-medium\">
                <i class=\"far fa-paper-plane\"></i>
              </span>
            </p>
          </div>
        </div>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 112
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 113
        echo "    <script
      type=\"text/javascript\"
      src=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js\"
    ></script>
    <script>
      document.addEventListener(\"DOMContentLoaded\", () => {
        const \$navbarBurgers = Array.prototype.slice.call(
          document.querySelectorAll(\".navbar-burger\"),
          0
        );

        if (\$navbarBurgers.length > 0) {
          \$navbarBurgers.forEach(el => {
            el.addEventListener(\"click\", () => {
              const target = el.dataset.target;
              const \$target = document.getElementById(target);

              el.classList.toggle(\"is-active\");
              \$target.classList.toggle(\"is-active\");
            });
          });
        }
      });

      document.addEventListener(\"DOMContentLoaded\", () => {
        (document.querySelectorAll(\".notification .delete\") || []).forEach(
          \$delete => {
            \$notification = \$delete.parentNode;
            \$delete.addEventListener(\"click\", () => {
              \$notification.parentNode.removeChild(\$notification);
            });
          }
        );
      });
    </script>
    ";
        // line 153
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html copy.twig";
    }

    public function getDebugInfo()
    {
        return array (  346 => 153,  309 => 113,  299 => 112,  257 => 76,  247 => 75,  229 => 72,  193 => 41,  187 => 38,  181 => 35,  160 => 17,  155 => 14,  145 => 13,  132 => 7,  122 => 6,  103 => 5,  91 => 154,  89 => 112,  84 => 109,  82 => 75,  78 => 73,  76 => 72,  69 => 67,  67 => 13,  61 => 9,  59 => 6,  55 => 5,  49 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
  <head>
    <meta charset=\"UTF-8\" />
    <title>{% block title %}Welcome!{% endblock %}</title>
    {% block stylesheets %}
    <link rel=\"stylesheet\" href=\"{{ asset('build/css/app.css') }}\" />
    {% endblock %}
  </head>
  <body>
    <section class=\"hero\">
      <div class=\"hero-head is-fixed-top\">
        {% block navbar %}
        <nav class=\"navbar\" role=\"navigation\" aria-label=\"main navigation\">
          <div class=\"navbar-brand\">
            <a class=\"navbar-item\" href=\"#\">
              <img src=\"{{ asset('img/logo_white.png') }}\" />
            </a>

            <a
              role=\"button\"
              class=\"navbar-burger burger\"
              aria-label=\"menu\"
              aria-expanded=\"false\"
              data-target=\"navbarBasicExample\"
            >
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
              <span aria-hidden=\"true\"></span>
            </a>
          </div>

          <div id=\"navbarBasicExample\" class=\"navbar-menu\">
            <div class=\"navbar-center is-size-6\">
              <a class=\"navbar-item\" href=\"{{ path('new_ad') }}\">
                Déposer une annonce
              </a>
              <a class=\"navbar-item\" href=\"{{ path('new_ad') }}\">
                Rechercher une annonce
              </a>
              <a class=\"navbar-item\" href=\"{{ path('new_ad') }}\">
                Mes annonces
              </a>
            </div>
            <div class=\"navbar-center-mobile is-hidden-desktop\"></div>

            <div class=\"navbar-end\">
              <div class=\"navbar-item\">
                <a href=\"#\">
                  <span class=\"icon is-medium\">
                    <i class=\"far fa-user is-size-6\"></i>
                  </span>
                  <span
                    class=\"icon is-small-desktop is-medium-mobile is-medium-tablet\"
                  >
                    <i class=\"far fa-comment-alt is-size-6\"></i>
                  </span>
                  <span class=\"icon is-medium\">
                    <i class=\"far fa-heart is-size-6\"></i>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </nav>
        {% endblock %}
      </div>
    </section>

    <section class=\"hero is-fullheight-with-navbar\">
      <div class=\"hero-body\">
        {% block body %}{% endblock %}
      </div>
      <div class=\"hero-footer\">
        {% block footer %}
        <div class=\"columns is-vcentered\">
          <div class=\"column\">
            <p
              class=\"has-text-left-desktop has-text-centered-mobile has-text-left-tablet\"
            >
              <a href=\"#\" class=\"is-size-7\">Mentions légales</a>
              |
              <a href=\"#\" class=\"is-size-7\">Protection des données</a>
              |
              <a href=\"#\" class=\"is-size-7\">CGV</a>
            </p>
          </div>
          <div class=\"column\">
            <p class=\"has-text-centered is-size-7\">@UFiX 2019</p>
          </div>
          <div class=\"column\">
            <p
              class=\"has-text-right-desktop has-text-centered-mobile is-size-7 has-text-right-tablet\"
            >
              Retrouvez-nous sur :
              <span class=\"icon is-medium\">
                <i class=\"fab fa-twitter\"></i>
              </span>
              <span class=\"icon is-small\">
                <i class=\"fab fa-facebook-f\"></i>
              </span>
              <span class=\"icon is-medium\">
                <i class=\"far fa-paper-plane\"></i>
              </span>
            </p>
          </div>
        </div>
        {% endblock %}
      </div>
    </section>

    {% block javascripts %}
    <script
      type=\"text/javascript\"
      src=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js\"
    ></script>
    <script>
      document.addEventListener(\"DOMContentLoaded\", () => {
        const \$navbarBurgers = Array.prototype.slice.call(
          document.querySelectorAll(\".navbar-burger\"),
          0
        );

        if (\$navbarBurgers.length > 0) {
          \$navbarBurgers.forEach(el => {
            el.addEventListener(\"click\", () => {
              const target = el.dataset.target;
              const \$target = document.getElementById(target);

              el.classList.toggle(\"is-active\");
              \$target.classList.toggle(\"is-active\");
            });
          });
        }
      });

      document.addEventListener(\"DOMContentLoaded\", () => {
        (document.querySelectorAll(\".notification .delete\") || []).forEach(
          \$delete => {
            \$notification = \$delete.parentNode;
            \$delete.addEventListener(\"click\", () => {
              \$notification.parentNode.removeChild(\$notification);
            });
          }
        );
      });
    </script>
    {#
    <script
      type=\"text/javascript\"
      src=\"{{ asset('build/js/app.js') }}\"
    ></script>
    #} {% endblock %}
  </body>
</html>
", "base.html copy.twig", "/home/amaurel/ufix/Ufix/templates/base.html copy.twig");
    }
}
