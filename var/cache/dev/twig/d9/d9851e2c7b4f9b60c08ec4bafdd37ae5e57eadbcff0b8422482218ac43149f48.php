<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact_seller_without_repair.html.twig */
class __TwigTemplate_7a507b9ec7aec1671d9d510e7799d8a3766025419b8d39fddac24cf67c337cd6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact_seller_without_repair.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contact_seller_without_repair.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "contact_seller_without_repair.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 2
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Contacter le vendeur</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "<div class=\"container\">
  <div class=\"card has-background-light\">
      <div class=\"has-text-centered\">
        <p class=\"title is-size-4\">Jacques POMMIER</p>
      </div>
      <div class=\"has-text-centered\">
        <p class=\"title is-size-5 has-text-grey\">GRADIGNAN - 33170</p>
      </div>

    <div class=\"card-content has-text-centered\">
      <div class=\"content\">
        <form action=\"\" >
          <div class=\"field margin-contact\">
            <div class=\"control\">
              <textarea class=\"textarea min-height-textarea\">
Bonjour, 
              
Je suis fortement intéressé par votre offre, est-elle toujours disponible ?
Si oui, quand et où pouvons-nous nous rencontrer ?

Cordialement,
Léo POMA
              </textarea>
            </div>
          </div>
        </form>
      </div>
    </div>

    <footer class=\"contact-footer\">
      <div class=\"columns is-marginless is-vcentered is-variable is-hidden-mobile\" >
        <div class=\"column is-1\">
          <img
            src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse-petit.png"), "html", null, true);
        echo "\"
            alt=\"image du téléphone\"
          />
        </div>
        <div class=\"column is-9\">
          <p class=\"has-text-white\">Iphone 4</p>
          <p class=\"has-text-grey\">Ecran cassé</p>
        </div>
        <div class=\"column\">
            <button class=\"button is-outlined is-rounded is-primary\">
              <span>Envoyer</span>
              <span class=\"icon\">
                <i class=\"fas fa-angle-right\"></i>
              </span> 
            </button>
        </div>
      </div>

      <div class=\"columns is-marginless is-vcentered is-variable is-mobile is-hidden-tablet is-hidden-desktop\" >
        <div class=\"column is-2\">
          <img
            src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse-petit.png"), "html", null, true);
        echo "\"
            alt=\"image du téléphone\"
          />
        </div>
        <div class=\"column is-6\">
          <p class=\"has-text-white\">Iphone 4</p>
          <p class=\"has-text-grey\">Ecran cassé</p>
        </div>
        <div class=\"column is-4\">
            <button class=\"button is-outlined is-rounded is-primary\">
              <span>Envoyer</span>
              <span class=\"icon\">
                <i class=\"fas fa-angle-right\"></i>
              </span> 
            </button>
        </div>
      </div>
    </footer>
    
  </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contact_seller_without_repair.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 61,  128 => 40,  93 => 7,  83 => 6,  68 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} {% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Contacter le vendeur</p>
</div>
{% endblock %} {% block body %}
<div class=\"container\">
  <div class=\"card has-background-light\">
      <div class=\"has-text-centered\">
        <p class=\"title is-size-4\">Jacques POMMIER</p>
      </div>
      <div class=\"has-text-centered\">
        <p class=\"title is-size-5 has-text-grey\">GRADIGNAN - 33170</p>
      </div>

    <div class=\"card-content has-text-centered\">
      <div class=\"content\">
        <form action=\"\" >
          <div class=\"field margin-contact\">
            <div class=\"control\">
              <textarea class=\"textarea min-height-textarea\">
Bonjour, 
              
Je suis fortement intéressé par votre offre, est-elle toujours disponible ?
Si oui, quand et où pouvons-nous nous rencontrer ?

Cordialement,
Léo POMA
              </textarea>
            </div>
          </div>
        </form>
      </div>
    </div>

    <footer class=\"contact-footer\">
      <div class=\"columns is-marginless is-vcentered is-variable is-hidden-mobile\" >
        <div class=\"column is-1\">
          <img
            src=\"{{ asset('img/ecran-casse-petit.png') }}\"
            alt=\"image du téléphone\"
          />
        </div>
        <div class=\"column is-9\">
          <p class=\"has-text-white\">Iphone 4</p>
          <p class=\"has-text-grey\">Ecran cassé</p>
        </div>
        <div class=\"column\">
            <button class=\"button is-outlined is-rounded is-primary\">
              <span>Envoyer</span>
              <span class=\"icon\">
                <i class=\"fas fa-angle-right\"></i>
              </span> 
            </button>
        </div>
      </div>

      <div class=\"columns is-marginless is-vcentered is-variable is-mobile is-hidden-tablet is-hidden-desktop\" >
        <div class=\"column is-2\">
          <img
            src=\"{{ asset('img/ecran-casse-petit.png') }}\"
            alt=\"image du téléphone\"
          />
        </div>
        <div class=\"column is-6\">
          <p class=\"has-text-white\">Iphone 4</p>
          <p class=\"has-text-grey\">Ecran cassé</p>
        </div>
        <div class=\"column is-4\">
            <button class=\"button is-outlined is-rounded is-primary\">
              <span>Envoyer</span>
              <span class=\"icon\">
                <i class=\"fas fa-angle-right\"></i>
              </span> 
            </button>
        </div>
      </div>
    </footer>
    
  </div>
</div>
{% endblock %}



", "contact_seller_without_repair.html.twig", "/home/amaurel/ufix/Ufix/templates/contact_seller_without_repair.html.twig");
    }
}
