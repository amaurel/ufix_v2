<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* TO_DELETE.html.twig */
class __TwigTemplate_76af16b9f68f0ffe512ae377c89ffbb55cee2ace6dea42a1e351b5eb2299bba0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "TO_DELETE.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "TO_DELETE.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "TO_DELETE.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 4
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
    <p class=\"is-size-3 has-text-centered\">Déposer une annonce</p>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "
<div class=\"container\">
<div class=\"tile is-ancestor\">
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Hello World</p>
        <p class=\"subtitle\">What is up?</p>
      </article>
    </div>
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Foo</p>
        <p class=\"subtitle\">Bar</p>
      </article>
    </div>
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Third column</p>
        <p class=\"subtitle\">With some content</p>
        <div class=\"content\">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
        </div>
      </article>
    </div>
  </div>
  
  <div class=\"tile is-ancestor\">
    <div class=\"tile is-vertical is-8\">
      <div class=\"tile\">
        <div class=\"tile is-parent is-vertical\">
          <article class=\"tile is-child box\">
            <p class=\"title\">Vertical tiles</p>
            <p class=\"subtitle\">Top box</p>
          </article>
          <article class=\"tile is-child box\">
            <p class=\"title\">Vertical tiles</p>
            <p class=\"subtitle\">Bottom box</p>
          </article>
        </div>
        <div class=\"tile is-parent\">
          <article class=\"tile is-child box\">
            <p class=\"title\">Middle box</p>
            <p class=\"subtitle\">With an image</p>
            <figure class=\"image is-4by3\">
              <img src=\"https://bulma.io/images/placeholders/640x480.png\">
            </figure>
          </article>
        </div>
      </div>
      <div class=\"tile is-parent\">
        <article class=\"tile is-child box\">
          <p class=\"title\">Wide column</p>
          <p class=\"subtitle\">Aligned with the right column</p>
          <div class=\"content\">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
          </div>
        </article>
      </div>
    </div>
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <div class=\"content\">
          <p class=\"title\">Tall column</p>
          <p class=\"subtitle\">With even more content</p>
          <div class=\"content\">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam.</p>
            <p>Suspendisse varius ligula in molestie lacinia. Maecenas varius eget ligula a sagittis. Pellentesque interdum, nisl nec interdum maximus, augue diam porttitor lorem, et sollicitudin felis neque sit amet erat. Maecenas imperdiet felis nisi, fringilla luctus felis hendrerit sit amet. Aenean vitae gravida diam, finibus dignissim turpis. Sed eget varius ligula, at volutpat tortor.</p>
            <p>Integer sollicitudin, tortor a mattis commodo, velit urna rhoncus erat, vitae congue lectus dolor consequat libero. Donec leo ligula, maximus et pellentesque sed, gravida a metus. Cras ullamcorper a nunc ac porta. Aliquam ut aliquet lacus, quis faucibus libero. Quisque non semper leo.</p>
          </div>
        </div>
      </article>
    </div>
  </div>
  
  <div class=\"tile is-ancestor\">
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Side column</p>
        <p class=\"subtitle\">With some content</p>
        <div class=\"content\">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
        </div>
      </article>
    </div>
    <div class=\"tile is-parent is-8\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Main column</p>
        <p class=\"subtitle\">With some content</p>
        <div class=\"content\">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
        </div>
      </article>
    </div>
  </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "TO_DELETE.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 12,  85 => 11,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
    <p class=\"is-size-3 has-text-centered\">Déposer une annonce</p>
</div>

{% endblock %}

{% block body %}

<div class=\"container\">
<div class=\"tile is-ancestor\">
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Hello World</p>
        <p class=\"subtitle\">What is up?</p>
      </article>
    </div>
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Foo</p>
        <p class=\"subtitle\">Bar</p>
      </article>
    </div>
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Third column</p>
        <p class=\"subtitle\">With some content</p>
        <div class=\"content\">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
        </div>
      </article>
    </div>
  </div>
  
  <div class=\"tile is-ancestor\">
    <div class=\"tile is-vertical is-8\">
      <div class=\"tile\">
        <div class=\"tile is-parent is-vertical\">
          <article class=\"tile is-child box\">
            <p class=\"title\">Vertical tiles</p>
            <p class=\"subtitle\">Top box</p>
          </article>
          <article class=\"tile is-child box\">
            <p class=\"title\">Vertical tiles</p>
            <p class=\"subtitle\">Bottom box</p>
          </article>
        </div>
        <div class=\"tile is-parent\">
          <article class=\"tile is-child box\">
            <p class=\"title\">Middle box</p>
            <p class=\"subtitle\">With an image</p>
            <figure class=\"image is-4by3\">
              <img src=\"https://bulma.io/images/placeholders/640x480.png\">
            </figure>
          </article>
        </div>
      </div>
      <div class=\"tile is-parent\">
        <article class=\"tile is-child box\">
          <p class=\"title\">Wide column</p>
          <p class=\"subtitle\">Aligned with the right column</p>
          <div class=\"content\">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
          </div>
        </article>
      </div>
    </div>
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <div class=\"content\">
          <p class=\"title\">Tall column</p>
          <p class=\"subtitle\">With even more content</p>
          <div class=\"content\">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam.</p>
            <p>Suspendisse varius ligula in molestie lacinia. Maecenas varius eget ligula a sagittis. Pellentesque interdum, nisl nec interdum maximus, augue diam porttitor lorem, et sollicitudin felis neque sit amet erat. Maecenas imperdiet felis nisi, fringilla luctus felis hendrerit sit amet. Aenean vitae gravida diam, finibus dignissim turpis. Sed eget varius ligula, at volutpat tortor.</p>
            <p>Integer sollicitudin, tortor a mattis commodo, velit urna rhoncus erat, vitae congue lectus dolor consequat libero. Donec leo ligula, maximus et pellentesque sed, gravida a metus. Cras ullamcorper a nunc ac porta. Aliquam ut aliquet lacus, quis faucibus libero. Quisque non semper leo.</p>
          </div>
        </div>
      </article>
    </div>
  </div>
  
  <div class=\"tile is-ancestor\">
    <div class=\"tile is-parent\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Side column</p>
        <p class=\"subtitle\">With some content</p>
        <div class=\"content\">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
        </div>
      </article>
    </div>
    <div class=\"tile is-parent is-8\">
      <article class=\"tile is-child box\">
        <p class=\"title\">Main column</p>
        <p class=\"subtitle\">With some content</p>
        <div class=\"content\">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
        </div>
      </article>
    </div>
  </div>
</div>

{% endblock %}

", "TO_DELETE.html.twig", "/home/amaurel/ufix/Ufix/templates/TO_DELETE.html.twig");
    }
}
