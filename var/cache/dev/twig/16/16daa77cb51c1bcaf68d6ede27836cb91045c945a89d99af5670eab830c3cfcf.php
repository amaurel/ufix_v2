<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home.html.twig */
class __TwigTemplate_78378246f5e5be6e79581a5c3e1813438c3509a2a2d66729096874139e127601 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base2.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home.html.twig"));

        $this->parent = $this->loadTemplate("base2.html.twig", "home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Accueil
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "<!-- Notification -->
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
  <button class=\"delete\"></button>
  <p>Vous êtes bien inscrit.</p>
</div>

<section class=\"hero is-fullheight-with-navbar\">
  <div class=\"hero-body\">
    <div class=\"container\">
      <!-- Top part -->
      <div class=\"columns is-vcentered p-3t\">
        <!-- Left part-->
        <div class=\"column is-5 is-offset-1-desktop is-offset-1-tablet is-offset-1-mobile landing-caption\">
          <h1 class=\"title is-1 is-bold is-spaced\">
            Faites renaître vos appareils de leurs cendres !
          </h1>
          <h2 class=\"subtitle is-5 is-dark\">Et ainsi participer à la réduction de l'empreinte environnementale.</h2>
          <p>
            <a class=\"button is-outlined is-primary scroll\" href=\"#third-block\">Connexion / Inscription</a>
          </p>
        </div>
        <!-- Image -->
        <div class=\"column is-5\">
          <img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/home.jpg"), "html", null, true);
        echo "\" alt=\"Image présentation UFix\" />
        </div>
      </div>
    </div>
  </div>
  <!-- Button to link next section -->
  <div class=\"hero-footer has-background-white\">
    <div class=\"container has-text-centered\">
      <a class=\"scroll\" href=\"#second-block\">
        <span class=\"icon\">
          <i class=\"fas fa-chevron-down\"></i>
        </span>
      </a>
    </div>
  </div>
</section>

<section id=\"second-block\" class=\"section is-medium\">
  <div class=\"container\">
    <!-- Presentation top-->
    <div class=\"title-wrapper has-text-centered\">
      <h2 class=\"title is-4\">Grâce à UFix tu peux :</h2>
      <h3 class=\"subtitle is-6\">*tout en préservant l'environnement</h3>
      <div class=\"divider is-centered\"></div>
    </div>
    <!-- Presentation of UFix -->
    <div class=\"content-wrapper\">
      <div class=\"columns\">
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Réparer</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon1.png"), "html", null, true);
        echo "\" alt=\"Icone réparer\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Tu souhaites réparer des appareils électroniques tels que des téléphones,
                ordinateurs ou tablettes ?</p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Réparer des appareils</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Acheter</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon2.png"), "html", null, true);
        echo "\" alt=\"Icone acheter\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Achètes des appareils électroniques usagés mais fonctionnels, à moindre coût.
              </p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Acheter un appareil</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Vendre</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon4.png"), "html", null, true);
        echo "\" alt=\"Icone vendre\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Vends tes appareils usagés et participe à la réduction de l'empreinte
                environnementale sur notre planète.</p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("newad"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Vendre un appareil</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Faire réparer</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/icon3.png"), "html", null, true);
        echo "\" alt=\"Icone faire réparer\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Viens faire réparer tes appareils cassés en toute simplicité et de façon
                responsable.</p>
            </div>
            <div class=\"card-action\">
              <a href=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("newad"), "html", null, true);
        echo "\" class=\"button is-outlined is-primary\">Faire réparer son appareil</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Inscription and connexion part -->
<section id=\"third-block\" class=\"section is-medium\">
  <div class=\"container\">
    <div class=\"columns is-vcentered\">
      <div class=\"column is-8 is-offset-2\">
        <div class=\"columns shadows-effect\">
          <!-- Connexion -->
          <div class=\"column is-4 has-text-centered sign-in-home\">
            <p class=\"title is-light\">Connexion</p>
            <form method=\"post\" action=\"\" class=\"form p-form\">
              <div class=\"field\">
                <p class=\"control has-icons-left has-icons-right\">
                  <input class=\"input\" type=\"email\" placeholder=\"Email\" required>
                  <span class=\"icon is-small is-left\">
                    <i class=\"fas fa-envelope\"></i>
                  </span>
                  <span class=\"icon is-small is-right\">
                    <i class=\"fas fa-check\"></i>
                  </span>
                </p>
              </div>
              <div class=\"field m-2b\">
                <p class=\"control has-icons-left\">
                  <input class=\"input\" type=\"password\" placeholder=\"Password\" required>
                  <span class=\"icon is-small is-left\">
                    <i class=\"fas fa-lock\"></i>
                  </span>
                </p>
              </div>
              <button class=\"button is-primary is-outlined\" href=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\" type=\"submit\">Se
                connecter</button>
            </form>
          </div>
          <!-- Innscription -->
          <div class=\"column is-8 has-text-centered sign-up-home\">
            <p class=\"title has-text-centered\">Inscription</p>
            <form method=\"post\" action=\"\" class=\"form\">
              <div class=\"columns\">
                <div class=\"column is-6\">
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Nom\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-user\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Prénom\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-user\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"email\" placeholder=\"Email\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-envelope\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"password\" placeholder=\"Mot de passe\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-lock\"></i>
                      </span>
                    </p>
                  </div>
                </div>
                <div class=\"column is-6\">
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Adresse\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-map-marker-alt\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Code postale\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-map-marker-alt\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Pays\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-flag\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field m-2b\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"text\" placeholder=\"Ville\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-building\"></i>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <label class=\"checkbox\">
                <input type=\"checkbox\" required>
                I agree to the <a href=\"#\">terms and conditions</a>
              </label>
              <br />
              <button class=\"button is-light is-outlined m-1t\" href=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("home"), "html", null, true);
        echo "\"
                type=\"submit\">M'inscrire</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 243,  273 => 160,  233 => 123,  223 => 116,  210 => 106,  200 => 99,  187 => 89,  177 => 82,  164 => 72,  154 => 65,  117 => 31,  92 => 8,  82 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base2.html.twig' %}

{% block title %}
\t{{ parent() }} - Accueil
{% endblock %}

{% block body %}
<!-- Notification -->
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
  <button class=\"delete\"></button>
  <p>Vous êtes bien inscrit.</p>
</div>

<section class=\"hero is-fullheight-with-navbar\">
  <div class=\"hero-body\">
    <div class=\"container\">
      <!-- Top part -->
      <div class=\"columns is-vcentered p-3t\">
        <!-- Left part-->
        <div class=\"column is-5 is-offset-1-desktop is-offset-1-tablet is-offset-1-mobile landing-caption\">
          <h1 class=\"title is-1 is-bold is-spaced\">
            Faites renaître vos appareils de leurs cendres !
          </h1>
          <h2 class=\"subtitle is-5 is-dark\">Et ainsi participer à la réduction de l'empreinte environnementale.</h2>
          <p>
            <a class=\"button is-outlined is-primary scroll\" href=\"#third-block\">Connexion / Inscription</a>
          </p>
        </div>
        <!-- Image -->
        <div class=\"column is-5\">
          <img src=\"{{ asset('img/home.jpg') }}\" alt=\"Image présentation UFix\" />
        </div>
      </div>
    </div>
  </div>
  <!-- Button to link next section -->
  <div class=\"hero-footer has-background-white\">
    <div class=\"container has-text-centered\">
      <a class=\"scroll\" href=\"#second-block\">
        <span class=\"icon\">
          <i class=\"fas fa-chevron-down\"></i>
        </span>
      </a>
    </div>
  </div>
</section>

<section id=\"second-block\" class=\"section is-medium\">
  <div class=\"container\">
    <!-- Presentation top-->
    <div class=\"title-wrapper has-text-centered\">
      <h2 class=\"title is-4\">Grâce à UFix tu peux :</h2>
      <h3 class=\"subtitle is-6\">*tout en préservant l'environnement</h3>
      <div class=\"divider is-centered\"></div>
    </div>
    <!-- Presentation of UFix -->
    <div class=\"content-wrapper\">
      <div class=\"columns\">
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Réparer</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"{{ asset('img/icon1.png') }}\" alt=\"Icone réparer\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Tu souhaites réparer des appareils électroniques tels que des téléphones,
                ordinateurs ou tablettes ?</p>
            </div>
            <div class=\"card-action\">
              <a href=\"{{ asset('home') }}\" class=\"button is-outlined is-primary\">Réparer des appareils</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Acheter</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"{{ asset('img/icon2.png') }}\" alt=\"Icone acheter\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Achètes des appareils électroniques usagés mais fonctionnels, à moindre coût.
              </p>
            </div>
            <div class=\"card-action\">
              <a href=\"{{ asset('home') }}\" class=\"button is-outlined is-primary\">Acheter un appareil</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Vendre</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"{{ asset('img/icon4.png') }}\" alt=\"Icone vendre\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Vends tes appareils usagés et participe à la réduction de l'empreinte
                environnementale sur notre planète.</p>
            </div>
            <div class=\"card-action\">
              <a href=\"{{ asset('newad') }}\" class=\"button is-outlined is-primary\">Vendre un appareil</a>
            </div>
          </div>
        </div>
        <div class=\"column is-one-quarter\">
          <div class=\"feature-card is-bordered has-text-centered\">
            <div class=\"card-title\">
              <h4>Faire réparer</h4>
            </div>
            <div class=\"card-icon\">
              <img class=\"image-home\" src=\"{{ asset('img/icon3.png') }}\" alt=\"Icone faire réparer\" />
            </div>
            <div class=\"card-text\">
              <p class=\"small-text-area\">Viens faire réparer tes appareils cassés en toute simplicité et de façon
                responsable.</p>
            </div>
            <div class=\"card-action\">
              <a href=\"{{ asset('newad') }}\" class=\"button is-outlined is-primary\">Faire réparer son appareil</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Inscription and connexion part -->
<section id=\"third-block\" class=\"section is-medium\">
  <div class=\"container\">
    <div class=\"columns is-vcentered\">
      <div class=\"column is-8 is-offset-2\">
        <div class=\"columns shadows-effect\">
          <!-- Connexion -->
          <div class=\"column is-4 has-text-centered sign-in-home\">
            <p class=\"title is-light\">Connexion</p>
            <form method=\"post\" action=\"\" class=\"form p-form\">
              <div class=\"field\">
                <p class=\"control has-icons-left has-icons-right\">
                  <input class=\"input\" type=\"email\" placeholder=\"Email\" required>
                  <span class=\"icon is-small is-left\">
                    <i class=\"fas fa-envelope\"></i>
                  </span>
                  <span class=\"icon is-small is-right\">
                    <i class=\"fas fa-check\"></i>
                  </span>
                </p>
              </div>
              <div class=\"field m-2b\">
                <p class=\"control has-icons-left\">
                  <input class=\"input\" type=\"password\" placeholder=\"Password\" required>
                  <span class=\"icon is-small is-left\">
                    <i class=\"fas fa-lock\"></i>
                  </span>
                </p>
              </div>
              <button class=\"button is-primary is-outlined\" href=\"{{ asset('home') }}\" type=\"submit\">Se
                connecter</button>
            </form>
          </div>
          <!-- Innscription -->
          <div class=\"column is-8 has-text-centered sign-up-home\">
            <p class=\"title has-text-centered\">Inscription</p>
            <form method=\"post\" action=\"\" class=\"form\">
              <div class=\"columns\">
                <div class=\"column is-6\">
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Nom\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-user\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Prénom\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-user\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"email\" placeholder=\"Email\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-envelope\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"password\" placeholder=\"Mot de passe\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-lock\"></i>
                      </span>
                    </p>
                  </div>
                </div>
                <div class=\"column is-6\">
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Adresse\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-map-marker-alt\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Code postale\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"fas fa-map-marker-alt\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field\">
                    <p class=\"control has-icons-left has-icons-right\">
                      <input class=\"input\" type=\"text\" placeholder=\"Pays\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-flag\"></i>
                      </span>
                    </p>
                  </div>
                  <div class=\"field m-2b\">
                    <p class=\"control has-icons-left\">
                      <input class=\"input\" type=\"text\" placeholder=\"Ville\" required>
                      <span class=\"icon is-small is-left\">
                        <i class=\"far fa-building\"></i>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <label class=\"checkbox\">
                <input type=\"checkbox\" required>
                I agree to the <a href=\"#\">terms and conditions</a>
              </label>
              <br />
              <button class=\"button is-light is-outlined m-1t\" href=\"{{ asset('home') }}\"
                type=\"submit\">M'inscrire</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{% endblock %}", "home.html.twig", "/home/amaurel/ufix/Ufix/templates/home.html.twig");
    }
}
