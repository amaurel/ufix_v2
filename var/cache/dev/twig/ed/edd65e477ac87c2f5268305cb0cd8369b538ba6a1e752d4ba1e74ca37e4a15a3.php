<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* new_ad.html.twig */
class __TwigTemplate_9ce37a4a02daf18a934c9d9097e015ca9fbb930d6dfcb48b290cf9fd7be82e94 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "new_ad.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "new_ad.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "new_ad.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Déposer une annonce
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Déposer une annonce</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
  <button class=\"delete\"></button>
  <p>Annonce publiée !</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 16
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 17
        echo "
<div class=\"container\">
  <div class=\"columns container2 is-multiline is-desktop p-3t\">
    <div class=\"column is-9-desktop is-12\">
      <!-- Information part (left)  -->
      <article class=\"tile is-child \">
        <p class=\"title has-text-centered\">Informations produit</p>
        <div class=\"field\">
          <label class=\"label\">Catégorie</label>
          <div class=\"control\">
            <div class=\"select\">
              <select>
                <option>Téléphone</option>
                <option>Tablette</option>
                <option>Ordinateur</option>
              </select>
            </div>
          </div>
        </div>
        <div class=\"field\">
          <label class=\"label\">Nom du produit</label>
          <div class=\"control\">
            <input class=\"input\" type=\"text\" placeholder=\"Iphone 4\" />
          </div>
        </div>
        <div class=\"field\">
          <label class=\"label\">Etat du produit</label>
          <div class=\"control\">
            <input class=\"input\" type=\"text\" placeholder=\"Ecran cassé\" />
          </div>
        </div>

        <div class=\"field\">
          <label class=\"label\">Description</label>
          <div class=\"control\">
            <textarea class=\"textarea\" placeholder=\"Je vend mon ...\"></textarea>
          </div>
        </div>

        <div class=\"field\">
          <div class=\"control\">
            <div class=\"field\">
              <input
                class=\"is-checkradio is-rtl has-background-color is-white\"
                id=\"exampleRtlRadioInline1\"
                type=\"radio\"
                name=\"exampleRtlRadioInline\"
                checked=\"checked\"
              />
              <label for=\"exampleRtlRadioInline1\">Vendre</label>
              <input
                class=\"is-checkradio is-rtl has-background-color is-white\"
                id=\"exampleRtlRadioInline2\"
                type=\"radio\"
                name=\"exampleRtlRadioInline\"
              />
              <label for=\"exampleRtlRadioInline2\">Réparer</label>
            </div>
          </div>
        </div>

        <div class=\"field prix m-2b\">
          <label class=\"label\">Prix</label>
          <div class=\"control has-icons-right\">
            <input class=\"input\" type=\"text\" placeholder=\"Prix\" />
            <span class=\"icon is-small is-right\">
              <i class=\"fas fa-euro-sign\"></i>
            </span>
          </div>
        </div>
      </article>
    </div>
    <!-- Images to upload -->
    <div
      class=\"column is-3-desktop is-8-mobile is-offset-2-mobile\"
    >
    <p class=\"title has-text-centered\">Images produit</p>
    ";
        // line 95
        echo "      <div class=\"avatar-upload\">
        <div class=\"avatar-edit\">
          <input type=\"file\" id=\"imageUpload\" accept=\".png, .jpg, .jpeg\" />
          <label for=\"imageUpload\"></label>
        </div>
        <div class=\"avatar-preview\">
          <div
            id=\"imagePreview\">
          </div>
        </div>
      </div>
    ";
        // line 107
        echo "    <div class=\"avatar-upload\">
        <div class=\"avatar-edit\">
          <input type=\"file\" id=\"imageUpload2\" accept=\".png, .jpg, .jpeg\" />
          <label for=\"imageUpload2\"></label>
        </div>
        <div class=\"avatar-preview\">
          <div
            id=\"imagePreview2\">
          </div>
        </div>
      </div>
      ";
        // line 119
        echo "      <div class=\"avatar-upload\">
          <div class=\"avatar-edit\">
            <input type=\"file\" id=\"imageUpload3\" accept=\".png, .jpg, .jpeg\" />
            <label for=\"imageUpload3\"></label>
          </div>
          <div class=\"avatar-preview\">
            <div
              id=\"imagePreview3\">
            </div>
          </div>
        </div>
    </div>
    <div class=\"column is-half is-offset-one-quarter has-text-centered\">
      <a class=\"button is-primary is-rounded is-outlined\" href=\"";
        // line 132
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("home_connected");
        echo "\" onclick=\"isSend()\">Publier</a>
    </div>
  </div>
</div>
<script>
  const fileInput = document.querySelector(\"#file-js-example input[type=file]\");
  fileInput.onchange = () => {
    if (fileInput.files.length > 0) {
      const fileName = document.querySelector(\"#file-js-example .file-name\");
      fileName.textContent = fileInput.files[0].name;
    }
  };
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "new_ad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 132,  227 => 119,  214 => 107,  201 => 95,  122 => 17,  112 => 16,  93 => 8,  83 => 7,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} 

{% block title %}
\t{{ parent() }} - Déposer une annonce
{% endblock %}

{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Déposer une annonce</p>
</div>
<div id=\"notif\" class=\"notification notif-fixed-top is-hidden\">
  <button class=\"delete\"></button>
  <p>Annonce publiée !</p>
</div>
{% endblock %} {% block body %}

<div class=\"container\">
  <div class=\"columns container2 is-multiline is-desktop p-3t\">
    <div class=\"column is-9-desktop is-12\">
      <!-- Information part (left)  -->
      <article class=\"tile is-child \">
        <p class=\"title has-text-centered\">Informations produit</p>
        <div class=\"field\">
          <label class=\"label\">Catégorie</label>
          <div class=\"control\">
            <div class=\"select\">
              <select>
                <option>Téléphone</option>
                <option>Tablette</option>
                <option>Ordinateur</option>
              </select>
            </div>
          </div>
        </div>
        <div class=\"field\">
          <label class=\"label\">Nom du produit</label>
          <div class=\"control\">
            <input class=\"input\" type=\"text\" placeholder=\"Iphone 4\" />
          </div>
        </div>
        <div class=\"field\">
          <label class=\"label\">Etat du produit</label>
          <div class=\"control\">
            <input class=\"input\" type=\"text\" placeholder=\"Ecran cassé\" />
          </div>
        </div>

        <div class=\"field\">
          <label class=\"label\">Description</label>
          <div class=\"control\">
            <textarea class=\"textarea\" placeholder=\"Je vend mon ...\"></textarea>
          </div>
        </div>

        <div class=\"field\">
          <div class=\"control\">
            <div class=\"field\">
              <input
                class=\"is-checkradio is-rtl has-background-color is-white\"
                id=\"exampleRtlRadioInline1\"
                type=\"radio\"
                name=\"exampleRtlRadioInline\"
                checked=\"checked\"
              />
              <label for=\"exampleRtlRadioInline1\">Vendre</label>
              <input
                class=\"is-checkradio is-rtl has-background-color is-white\"
                id=\"exampleRtlRadioInline2\"
                type=\"radio\"
                name=\"exampleRtlRadioInline\"
              />
              <label for=\"exampleRtlRadioInline2\">Réparer</label>
            </div>
          </div>
        </div>

        <div class=\"field prix m-2b\">
          <label class=\"label\">Prix</label>
          <div class=\"control has-icons-right\">
            <input class=\"input\" type=\"text\" placeholder=\"Prix\" />
            <span class=\"icon is-small is-right\">
              <i class=\"fas fa-euro-sign\"></i>
            </span>
          </div>
        </div>
      </article>
    </div>
    <!-- Images to upload -->
    <div
      class=\"column is-3-desktop is-8-mobile is-offset-2-mobile\"
    >
    <p class=\"title has-text-centered\">Images produit</p>
    {# AVATAR 1 #}
      <div class=\"avatar-upload\">
        <div class=\"avatar-edit\">
          <input type=\"file\" id=\"imageUpload\" accept=\".png, .jpg, .jpeg\" />
          <label for=\"imageUpload\"></label>
        </div>
        <div class=\"avatar-preview\">
          <div
            id=\"imagePreview\">
          </div>
        </div>
      </div>
    {# AVATAR 2 #}
    <div class=\"avatar-upload\">
        <div class=\"avatar-edit\">
          <input type=\"file\" id=\"imageUpload2\" accept=\".png, .jpg, .jpeg\" />
          <label for=\"imageUpload2\"></label>
        </div>
        <div class=\"avatar-preview\">
          <div
            id=\"imagePreview2\">
          </div>
        </div>
      </div>
      {# AVATAR 3 #}
      <div class=\"avatar-upload\">
          <div class=\"avatar-edit\">
            <input type=\"file\" id=\"imageUpload3\" accept=\".png, .jpg, .jpeg\" />
            <label for=\"imageUpload3\"></label>
          </div>
          <div class=\"avatar-preview\">
            <div
              id=\"imagePreview3\">
            </div>
          </div>
        </div>
    </div>
    <div class=\"column is-half is-offset-one-quarter has-text-centered\">
      <a class=\"button is-primary is-rounded is-outlined\" href=\"{{ path(\"home_connected\") }}\" onclick=\"isSend()\">Publier</a>
    </div>
  </div>
</div>
<script>
  const fileInput = document.querySelector(\"#file-js-example input[type=file]\");
  fileInput.onchange = () => {
    if (fileInput.files.length > 0) {
      const fileName = document.querySelector(\"#file-js-example .file-name\");
      fileName.textContent = fileInput.files[0].name;
    }
  };
</script>
{% endblock %}
", "new_ad.html.twig", "/home/amaurel/dawin_plans/ufix/Ufix/templates/new_ad.html.twig");
    }
}
