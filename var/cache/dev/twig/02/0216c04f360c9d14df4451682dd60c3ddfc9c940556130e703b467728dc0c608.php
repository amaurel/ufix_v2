<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* legal_mentions.html.twig */
class __TwigTemplate_b23c44b15f7ddbd5bf3774305adeef894d36c3ded2882bb643528a0b9626f343 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "legal_mentions.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "legal_mentions.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "legal_mentions.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\tMentions Légales
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 8
        echo "\t";
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"title has-text-centered\">Mentions Légales</p>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "<div class=\"container\">
    <div class=\"card has-background-light\">
        <div class=\"card-content\">
\t\t\t<div class=\"legal-mentions has-text-centered\">
\t\t\t\t<p class=\"title is-size-4 has-text-centered\">Informations légales</p>
\t\t\t\t<p>Merci de lire attentivement les présentes modalités d'utilisation du présent site avant de le parcourir. En vous connectant 
\t\t\t\t\t\t\t\t\t\t\t\t\tsur ce site, vous acceptez sans réserve les présentes modalités.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Editeur du site</p>
\t\t\t\t<p>
\t\t\t\t\t<strong>Site Internet Qualité
\t\t\t\t\t</strong>
\t\t\t\t\t<br>
\t\t\t\t\tpar UFix
\t\t\t\t\t<br>
\t\t\t\t\tDépartement informatique
\t\t\t\t\t<br>
\t\t\t\t\tIUT de Bordeaux, 15, rue Naudet, CS 10207
\t\t\t\t\t<br>
\t\t\t\t\t33175 Gradignan Cedex,
\t\t\t\t\t<br>
\t\t\t\t\tFrance
\t\t\t\t\t<br>
\t\t\t\t\tTél. : + 33 (0)6 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\tFax : + 33 (0)5 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\thttps://ufix.fr
\t\t\t\t\t<br>
\t\t\t\t\tUFix est une entreprise factice pour un projet étudiant.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Conditions d'utilisation</p>
\t\t\t\t<p>Le site accessible par les url suivants : https://u-fix.fr/ est exploité dans le respect de la législation française. L'utilisation 
\t\t\t\t\t\t\t\t\t\t\t\t\tde ce site est régie par les présentes conditions générales. En utilisant le site, vous reconnaissez avoir pris connaissance 
\t\t\t\t\t\t\t\t\t\t\t\t\tde ces conditions et les avoir acceptées. Celles-ci pourront êtres modifiées à tout moment et sans préavis par la société UFix.\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tUFix ne saurait être tenu pour responsable en aucune manière d’une mauvaise utilisation du service.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Responsable éditorial</p>
\t\t\t\t<p>Département informatique
\t\t\t\t\t<br>
\t\t\t\t\tIUT de Bordeaux, 15, rue Naudet, CS 10207
\t\t\t\t\t<br>
\t\t\t\t\t33175 Gradignan Cedex,
\t\t\t\t\t<br>
\t\t\t\t\tFrance
\t\t\t\t\t<br>
\t\t\t\t\tTél. : + 33 (0)6 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\tFax : + 33 (0)5 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\thttps://ufix.fr
\t\t\t\t\t<br>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Limitation de responsabilité</p>
\t\t\t\t<p>Les informations contenues sur ce site sont aussi précises que possibles et le site est périodiquement remis à jour, mais peut toutefois 
\t\t\t\t\t\t\t\t\tcontenir des inexactitudes, des omissions ou des lacunes. Si vous constatez une lacune, erreur ou ce qui paraît être un dysfonctionnement, 
\t\t\t\t\t\t\t\t\tmerci de bien vouloir le signaler par email en décrivant le problème de la manière la plus précise possible (page posant problème, action \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tdéclenchante, type d’ordinateur et de navigateur utilisé, …).
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tTout contenu téléchargé se fait aux risques et périls de l'utilisateur et sous sa seule responsabilité. En conséquence, UFix ne saurait être
\t\t\t\t\t\t\t\t\t tenu responsable d'un quelconque dommage subi par l'ordinateur de l'utilisateur ou d'une quelconque perte de données consécutives 
\t\t\t\t\t\t\t\t\t au téléchargement.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes photos sont non contractuelles.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes liens hypertextes mis en place dans le cadre du présent site internet en direction d'autres ressources présentes sur le réseau Internet 
\t\t\t\t\t\t\t\t\tne sauraient engager la responsabilité de UFix.
\t\t\t\t</p>
\t\t\t</div>
\t
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Litiges</p>
\t\t\t\t<p>Les présentes conditions sont régies par les lois françaises et toute contestation ou litiges qui pourraient naître de l'interprétation ou de 
\t\t\t\t\t\t\t\tl'exécution de celles-ci seront de la compétence exclusive des tribunaux dont dépend le siège social de la société UFix. La langue de 
\t\t\t\t\t\t\t\tréférence, pour le règlement de contentieux éventuels, est le français.
\t\t\t\t</p>
\t\t\t</div>
\t
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Déclaration à la CNIL</p>
\t\t\t\t<p>Conformément à la loi 78-17 du 6 janvier 1978 (modifiée par la loi 2004-801 du 6 août 2004 relative à la protection des personnes physiques à 
\t\t\t\t\t\t\t\tl'égard des traitements de données à caractère personnel) relative à l'informatique, aux fichiers et aux libertés, le site a fait l'objet 
\t\t\t\t\t\t\t\td'une déclaration auprès de la Commission nationale de l'informatique et des libertés (www.cnil.fr).
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Droits d'accèss</p>
\t\t\t\t<p>En application de cette loi, les internautes disposent d’un droit d’accès, de rectification, de modification et de suppression concernant
\t\t\t\t\t\t\t\t les données qui les concernent personnellement. Ce droit peut être exercé par voie postale auprès de Ufix Département informatique IUT 
\t\t\t\t\t\t\t\t de Bordeaux, 15, rue Naudet, CS 10207 33175 Gradignan Cedex ou par voie électronique à l’adresse email suivante : ufixfr@gmail.com.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes informations personnelles collectées ne sont en aucun cas confiées à des tiers hormis pour l’éventuelle bonne exécution de la 
\t\t\t\t\t\t\t\tprestation commandée par l’internaute.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Confidentialité</p>
\t\t\t\t<p>Vos données personnelles sont confidentielles et ne seront en aucun cas communiquées à des tiers hormis pour la bonne exécution de la prestation.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t
\t\t\t\t<p>Tout le contenu du présent site, incluant, de façon non limitative, les graphismes, images, textes, vidéos, animations, sons, logos, gifs et 
\t\t\t\t\t\t\t\ticônes ainsi que leur mise en forme sont la propriété exclusive de la société UFix à l'exception des marques, logos ou contenus appartenant 
\t\t\t\t\t\t\t\tà d'autres sociétés partenaires ou auteurs.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tToute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces différents éléments 
\t\t\t\t\t\t\t\t\test strictement interdite sans l'accord exprès par écrit de UFix. Cette représentation ou reproduction, par quelque procédé que ce soit, 
\t\t\t\t\t\t\t\t\tconstitue une contrefaçon sanctionnée par les articles L.3335-2 et suivants du Code de la propriété intellectuelle. Le non-respect de cette 
\t\t\t\t\t\t\t\t\tinterdiction constitue une contrefaçon pouvant engager la responsabilité civile et pénale du contrefacteur. En outre, les propriétaires des 
\t\t\t\t\t\t\t\t\tContenus copiés pourraient intenter une action en justice à votre encontre.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tUFix est identiquement propriétaire des \"droits des producteurs de bases de données\" visés au Livre III, Titre IV, du Code de la Propriété 
\t\t\t\t\t\t\t\t\tIntellectuelle (loi n° 98-536 du 1er juillet 1998) relative aux droits d'auteur et aux bases de données.
\t\t\t\t\t<br><br>
\t\t\t\t\tLes utilisateurs et visiteurs du site internet peuvent mettre en place un hyperlien en direction de ce site, mais uniquement en direction 
\t\t\t\t\t\t\t\t\tde la page d’accueil, accessible à l’URL suivante : https://u-fix.fr, à condition que ce lien s’ouvre dans une nouvelle fenêtre. En 
\t\t\t\t\t\t\t\t\tparticulier un lien vers une sous page (« lien profond ») est interdit, ainsi que l’ouverture du présent site au sein d’un cadre 
\t\t\t\t\t\t\t\t\t(« framing »), sauf l'autorisation expresse et préalable de UFix.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tPour toute demande d'autorisation ou d'information, veuillez nous contacter par email :
\t\t\t\t\t<strong>
\t\t\t\t\t\tufixfr@gmail.com</strong>. Des conditions spécifiques sont prévues pour la presse.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Hébérgeur</p>
\t\t\t\t<p>IUT de Bordeaux, Département Informatique</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Conditions de service</p>
\t\t\t\t<p>Ce site est proposé en langages HTML5 et CSS3, pour un meilleur confort d'utilisation et un graphisme plus agréable, 
\t\t\t\t\t\t\t\tnous vous recommandons de recourir à des navigateurs modernes comme Safari, Firefox, Chrome,...</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Informations et exclusion</p>
\t\t\t\t<p>Ufix met en œuvre tous les moyens dont elle dispose, pour assurer une information fiable et une mise à jour fiable de ses
\t\t\t\t\t\t\t\tsites internet. Toutefois, des erreurs ou omissions peuvent survenir. L'internaute devra donc s'assurer de l'exactitude des
\t\t\t\t\t\t\t\tinformations auprès de UFix, et signaler toutes modifications du site qu'il jugerait utile. UFix n'est en aucun cas responsable de
\t\t\t\t\t\t\t\tl'utilisation faite de ces informations, et de tout préjudice direct ou indirect pouvant en découler.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Cookies</p>
\t\t\t\t<p>Pour des besoins de statistiques et d'affichage, le présent site utilise des cookies. Il s'agit de petits fichiers textes stockés 
\t\t\t\t\t\t\t\tsur votre disque dur afin d'enregistrer des données techniques sur votre navigation. Certaines parties de ce site ne peuvent être 
\t\t\t\t\t\t\t\tfonctionnelle sans l’acceptation de cookies.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Liens hypertextes</p>
\t\t\t\t<p>Les sites internet de UFix peuvent offrir des liens vers d’autres sites internet ou d’autres ressources disponibles sur Internet.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tUFix ne dispose d'aucun moyen pour contrôler les sites en connexion avec ses sites internet. UFix ne répond pas de la disponibilité 
\t\t\t\t\t\t\t\t\tde tels sites et sources externes, ni ne la garantit. Elle ne peut être tenue pour responsable de tout dommage, de quelque nature que 
\t\t\t\t\t\t\t\t\tce soit, résultant du contenu de ces sites ou sources externes, et notamment des informations, produits ou services qu’ils proposent, 
\t\t\t\t\t\t\t\t\tou de tout usage qui peut être fait de ces éléments. Les risques liés à cette utilisation incombent pleinement à l'internaute, qui doit 
\t\t\t\t\t\t\t\t\tse conformer à leurs conditions d'utilisation.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes utilisateurs, les abonnés et les visiteurs des sites internet de UFix ne peuvent mettre en place un hyperlien en direction de 
\t\t\t\t\t\t\t\t\tce site sans l'autorisation expresse et préalable de UFix.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tDans l'hypothèse où un utilisateur ou visiteur souhaiterait mettre en place un hyperlien en direction d’un des sites internet de UFix, 
\t\t\t\t\t\t\t\t\til lui appartiendra d'adresser un email accessible sur le site afin de formuler sa demande de mise en place d'un hyperlien. UFix se réserve 
\t\t\t\t\t\t\t\t\tle droit d’accepter ou de refuser un hyperlien sans avoir à en justifier sa décision.
\t\t\t\t\t<br>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Recherche</p>
\t\t\t\t<p>En outre, le renvoi sur un site internet pour compléter une information recherchée ne signifie en aucune façon que UFix reconnaît ou accepte quelque 
\t\t\t\t\t\t\t\tresponsabilité quant à la teneur ou à l'utilisation dudit site.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Précautions d'usage</p>
\t\t\t\t<p>Il vous incombe par conséquent de prendre les précautions d'usage nécessaires pour vous assurer que ce que vous choisissez d'utiliser ne soit pas entaché 
\t\t\t\t\t\t\t\td'erreurs voire d'éléments de nature destructrice tels que virus, trojans, etc....</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Responsabilité</p>
\t\t\t\t<p>Aucune autre garantie n'est accordée au client, auquel incombe l'obligation de formuler clairement ses besoins et le devoir de 
\t\t\t\t\t\t\t\ts'informer. Si des informations fournies par UFix apparaissent inexactes, il appartiendra au client de procéder lui-même à 
\t\t\t\t\t\t\t\ttoutes vérifications de la cohérence ou de la vraisemblance des résultats obtenus. UFix ne sera en aucune façon responsable 
\t\t\t\t\t\t\t\tvis à vis des tiers de l'utilisation par le client des informations ou de leur absence contenues dans ses produits y compris 
\t\t\t\t\t\t\t\tun de ses sites Internet.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Contactez-nous</p>
\t\t\t\t<p>UFix est à votre disposition pour tous vos commentaires ou suggestions. Vous pouvez nous écrire en français par courrier 
\t\t\t\t\t\t\t\télectronique à : ufixfr@gmail.com</p>
\t\t\t</div>
\t\t\t
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "legal_mentions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 15,  107 => 14,  91 => 8,  81 => 7,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
\tMentions Légales
{% endblock %}

{% block navbar %}
\t{{ parent() }}
\t<div class=\"container-is-fluid extended\">
\t\t<p class=\"title has-text-centered\">Mentions Légales</p>
\t</div>
{% endblock %}

{% block body %}
<div class=\"container\">
    <div class=\"card has-background-light\">
        <div class=\"card-content\">
\t\t\t<div class=\"legal-mentions has-text-centered\">
\t\t\t\t<p class=\"title is-size-4 has-text-centered\">Informations légales</p>
\t\t\t\t<p>Merci de lire attentivement les présentes modalités d'utilisation du présent site avant de le parcourir. En vous connectant 
\t\t\t\t\t\t\t\t\t\t\t\t\tsur ce site, vous acceptez sans réserve les présentes modalités.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Editeur du site</p>
\t\t\t\t<p>
\t\t\t\t\t<strong>Site Internet Qualité
\t\t\t\t\t</strong>
\t\t\t\t\t<br>
\t\t\t\t\tpar UFix
\t\t\t\t\t<br>
\t\t\t\t\tDépartement informatique
\t\t\t\t\t<br>
\t\t\t\t\tIUT de Bordeaux, 15, rue Naudet, CS 10207
\t\t\t\t\t<br>
\t\t\t\t\t33175 Gradignan Cedex,
\t\t\t\t\t<br>
\t\t\t\t\tFrance
\t\t\t\t\t<br>
\t\t\t\t\tTél. : + 33 (0)6 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\tFax : + 33 (0)5 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\thttps://ufix.fr
\t\t\t\t\t<br>
\t\t\t\t\tUFix est une entreprise factice pour un projet étudiant.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Conditions d'utilisation</p>
\t\t\t\t<p>Le site accessible par les url suivants : https://u-fix.fr/ est exploité dans le respect de la législation française. L'utilisation 
\t\t\t\t\t\t\t\t\t\t\t\t\tde ce site est régie par les présentes conditions générales. En utilisant le site, vous reconnaissez avoir pris connaissance 
\t\t\t\t\t\t\t\t\t\t\t\t\tde ces conditions et les avoir acceptées. Celles-ci pourront êtres modifiées à tout moment et sans préavis par la société UFix.\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tUFix ne saurait être tenu pour responsable en aucune manière d’une mauvaise utilisation du service.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Responsable éditorial</p>
\t\t\t\t<p>Département informatique
\t\t\t\t\t<br>
\t\t\t\t\tIUT de Bordeaux, 15, rue Naudet, CS 10207
\t\t\t\t\t<br>
\t\t\t\t\t33175 Gradignan Cedex,
\t\t\t\t\t<br>
\t\t\t\t\tFrance
\t\t\t\t\t<br>
\t\t\t\t\tTél. : + 33 (0)6 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\tFax : + 33 (0)5 00 00 00 00
\t\t\t\t\t<br>
\t\t\t\t\thttps://ufix.fr
\t\t\t\t\t<br>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Limitation de responsabilité</p>
\t\t\t\t<p>Les informations contenues sur ce site sont aussi précises que possibles et le site est périodiquement remis à jour, mais peut toutefois 
\t\t\t\t\t\t\t\t\tcontenir des inexactitudes, des omissions ou des lacunes. Si vous constatez une lacune, erreur ou ce qui paraît être un dysfonctionnement, 
\t\t\t\t\t\t\t\t\tmerci de bien vouloir le signaler par email en décrivant le problème de la manière la plus précise possible (page posant problème, action \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tdéclenchante, type d’ordinateur et de navigateur utilisé, …).
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tTout contenu téléchargé se fait aux risques et périls de l'utilisateur et sous sa seule responsabilité. En conséquence, UFix ne saurait être
\t\t\t\t\t\t\t\t\t tenu responsable d'un quelconque dommage subi par l'ordinateur de l'utilisateur ou d'une quelconque perte de données consécutives 
\t\t\t\t\t\t\t\t\t au téléchargement.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes photos sont non contractuelles.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes liens hypertextes mis en place dans le cadre du présent site internet en direction d'autres ressources présentes sur le réseau Internet 
\t\t\t\t\t\t\t\t\tne sauraient engager la responsabilité de UFix.
\t\t\t\t</p>
\t\t\t</div>
\t
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Litiges</p>
\t\t\t\t<p>Les présentes conditions sont régies par les lois françaises et toute contestation ou litiges qui pourraient naître de l'interprétation ou de 
\t\t\t\t\t\t\t\tl'exécution de celles-ci seront de la compétence exclusive des tribunaux dont dépend le siège social de la société UFix. La langue de 
\t\t\t\t\t\t\t\tréférence, pour le règlement de contentieux éventuels, est le français.
\t\t\t\t</p>
\t\t\t</div>
\t
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Déclaration à la CNIL</p>
\t\t\t\t<p>Conformément à la loi 78-17 du 6 janvier 1978 (modifiée par la loi 2004-801 du 6 août 2004 relative à la protection des personnes physiques à 
\t\t\t\t\t\t\t\tl'égard des traitements de données à caractère personnel) relative à l'informatique, aux fichiers et aux libertés, le site a fait l'objet 
\t\t\t\t\t\t\t\td'une déclaration auprès de la Commission nationale de l'informatique et des libertés (www.cnil.fr).
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Droits d'accèss</p>
\t\t\t\t<p>En application de cette loi, les internautes disposent d’un droit d’accès, de rectification, de modification et de suppression concernant
\t\t\t\t\t\t\t\t les données qui les concernent personnellement. Ce droit peut être exercé par voie postale auprès de Ufix Département informatique IUT 
\t\t\t\t\t\t\t\t de Bordeaux, 15, rue Naudet, CS 10207 33175 Gradignan Cedex ou par voie électronique à l’adresse email suivante : ufixfr@gmail.com.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes informations personnelles collectées ne sont en aucun cas confiées à des tiers hormis pour l’éventuelle bonne exécution de la 
\t\t\t\t\t\t\t\tprestation commandée par l’internaute.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Confidentialité</p>
\t\t\t\t<p>Vos données personnelles sont confidentielles et ne seront en aucun cas communiquées à des tiers hormis pour la bonne exécution de la prestation.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t
\t\t\t\t<p>Tout le contenu du présent site, incluant, de façon non limitative, les graphismes, images, textes, vidéos, animations, sons, logos, gifs et 
\t\t\t\t\t\t\t\ticônes ainsi que leur mise en forme sont la propriété exclusive de la société UFix à l'exception des marques, logos ou contenus appartenant 
\t\t\t\t\t\t\t\tà d'autres sociétés partenaires ou auteurs.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tToute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces différents éléments 
\t\t\t\t\t\t\t\t\test strictement interdite sans l'accord exprès par écrit de UFix. Cette représentation ou reproduction, par quelque procédé que ce soit, 
\t\t\t\t\t\t\t\t\tconstitue une contrefaçon sanctionnée par les articles L.3335-2 et suivants du Code de la propriété intellectuelle. Le non-respect de cette 
\t\t\t\t\t\t\t\t\tinterdiction constitue une contrefaçon pouvant engager la responsabilité civile et pénale du contrefacteur. En outre, les propriétaires des 
\t\t\t\t\t\t\t\t\tContenus copiés pourraient intenter une action en justice à votre encontre.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tUFix est identiquement propriétaire des \"droits des producteurs de bases de données\" visés au Livre III, Titre IV, du Code de la Propriété 
\t\t\t\t\t\t\t\t\tIntellectuelle (loi n° 98-536 du 1er juillet 1998) relative aux droits d'auteur et aux bases de données.
\t\t\t\t\t<br><br>
\t\t\t\t\tLes utilisateurs et visiteurs du site internet peuvent mettre en place un hyperlien en direction de ce site, mais uniquement en direction 
\t\t\t\t\t\t\t\t\tde la page d’accueil, accessible à l’URL suivante : https://u-fix.fr, à condition que ce lien s’ouvre dans une nouvelle fenêtre. En 
\t\t\t\t\t\t\t\t\tparticulier un lien vers une sous page (« lien profond ») est interdit, ainsi que l’ouverture du présent site au sein d’un cadre 
\t\t\t\t\t\t\t\t\t(« framing »), sauf l'autorisation expresse et préalable de UFix.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tPour toute demande d'autorisation ou d'information, veuillez nous contacter par email :
\t\t\t\t\t<strong>
\t\t\t\t\t\tufixfr@gmail.com</strong>. Des conditions spécifiques sont prévues pour la presse.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Hébérgeur</p>
\t\t\t\t<p>IUT de Bordeaux, Département Informatique</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Conditions de service</p>
\t\t\t\t<p>Ce site est proposé en langages HTML5 et CSS3, pour un meilleur confort d'utilisation et un graphisme plus agréable, 
\t\t\t\t\t\t\t\tnous vous recommandons de recourir à des navigateurs modernes comme Safari, Firefox, Chrome,...</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Informations et exclusion</p>
\t\t\t\t<p>Ufix met en œuvre tous les moyens dont elle dispose, pour assurer une information fiable et une mise à jour fiable de ses
\t\t\t\t\t\t\t\tsites internet. Toutefois, des erreurs ou omissions peuvent survenir. L'internaute devra donc s'assurer de l'exactitude des
\t\t\t\t\t\t\t\tinformations auprès de UFix, et signaler toutes modifications du site qu'il jugerait utile. UFix n'est en aucun cas responsable de
\t\t\t\t\t\t\t\tl'utilisation faite de ces informations, et de tout préjudice direct ou indirect pouvant en découler.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Cookies</p>
\t\t\t\t<p>Pour des besoins de statistiques et d'affichage, le présent site utilise des cookies. Il s'agit de petits fichiers textes stockés 
\t\t\t\t\t\t\t\tsur votre disque dur afin d'enregistrer des données techniques sur votre navigation. Certaines parties de ce site ne peuvent être 
\t\t\t\t\t\t\t\tfonctionnelle sans l’acceptation de cookies.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Liens hypertextes</p>
\t\t\t\t<p>Les sites internet de UFix peuvent offrir des liens vers d’autres sites internet ou d’autres ressources disponibles sur Internet.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tUFix ne dispose d'aucun moyen pour contrôler les sites en connexion avec ses sites internet. UFix ne répond pas de la disponibilité 
\t\t\t\t\t\t\t\t\tde tels sites et sources externes, ni ne la garantit. Elle ne peut être tenue pour responsable de tout dommage, de quelque nature que 
\t\t\t\t\t\t\t\t\tce soit, résultant du contenu de ces sites ou sources externes, et notamment des informations, produits ou services qu’ils proposent, 
\t\t\t\t\t\t\t\t\tou de tout usage qui peut être fait de ces éléments. Les risques liés à cette utilisation incombent pleinement à l'internaute, qui doit 
\t\t\t\t\t\t\t\t\tse conformer à leurs conditions d'utilisation.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tLes utilisateurs, les abonnés et les visiteurs des sites internet de UFix ne peuvent mettre en place un hyperlien en direction de 
\t\t\t\t\t\t\t\t\tce site sans l'autorisation expresse et préalable de UFix.
\t\t\t\t\t<br>
\t\t\t\t\t<br>
\t\t\t\t\tDans l'hypothèse où un utilisateur ou visiteur souhaiterait mettre en place un hyperlien en direction d’un des sites internet de UFix, 
\t\t\t\t\t\t\t\t\til lui appartiendra d'adresser un email accessible sur le site afin de formuler sa demande de mise en place d'un hyperlien. UFix se réserve 
\t\t\t\t\t\t\t\t\tle droit d’accepter ou de refuser un hyperlien sans avoir à en justifier sa décision.
\t\t\t\t\t<br>
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Recherche</p>
\t\t\t\t<p>En outre, le renvoi sur un site internet pour compléter une information recherchée ne signifie en aucune façon que UFix reconnaît ou accepte quelque 
\t\t\t\t\t\t\t\tresponsabilité quant à la teneur ou à l'utilisation dudit site.</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Précautions d'usage</p>
\t\t\t\t<p>Il vous incombe par conséquent de prendre les précautions d'usage nécessaires pour vous assurer que ce que vous choisissez d'utiliser ne soit pas entaché 
\t\t\t\t\t\t\t\td'erreurs voire d'éléments de nature destructrice tels que virus, trojans, etc....</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Responsabilité</p>
\t\t\t\t<p>Aucune autre garantie n'est accordée au client, auquel incombe l'obligation de formuler clairement ses besoins et le devoir de 
\t\t\t\t\t\t\t\ts'informer. Si des informations fournies par UFix apparaissent inexactes, il appartiendra au client de procéder lui-même à 
\t\t\t\t\t\t\t\ttoutes vérifications de la cohérence ou de la vraisemblance des résultats obtenus. UFix ne sera en aucune façon responsable 
\t\t\t\t\t\t\t\tvis à vis des tiers de l'utilisation par le client des informations ou de leur absence contenues dans ses produits y compris 
\t\t\t\t\t\t\t\tun de ses sites Internet.
\t\t\t\t</p>
\t\t\t</div>
\t\t\t<div class=\"legal-mentions\">
\t\t\t\t<p class=\"title is-size-5 is-uppercase\">Contactez-nous</p>
\t\t\t\t<p>UFix est à votre disposition pour tous vos commentaires ou suggestions. Vous pouvez nous écrire en français par courrier 
\t\t\t\t\t\t\t\télectronique à : ufixfr@gmail.com</p>
\t\t\t</div>
\t\t\t
        </div>
    </div>
</div>
{% endblock %}
", "legal_mentions.html.twig", "/home/amaurel/ufix/Ufix/templates/legal_mentions.html.twig");
    }
}
