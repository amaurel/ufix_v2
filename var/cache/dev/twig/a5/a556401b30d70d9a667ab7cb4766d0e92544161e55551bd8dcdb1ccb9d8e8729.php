<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ads_saved.html.twig */
class __TwigTemplate_da3ac75d4cb0fcd4d7cbb0520f3f14cf17eaf326d57b97d961509bc95a86c49d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'navbar' => [$this, 'block_navbar'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ads_saved.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ads_saved.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "ads_saved.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "\t";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Annonces sauvegardées
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "navbar"));

        // line 8
        $this->displayParentBlock("navbar", $context, $blocks);
        echo "
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Annonces sauvegardées</p>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 12
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 13
        echo "<div class=\"container is-fluid-without-padding p-3t\">
  <div class=\"columns is-centered\">
    <div class=\"column is-half\">
      <article class=\"panel\">
        <!-- Top of card -->
        <p class=\"panel-tabs\">
          <a class=\"is-active\">Tous</a>
          <a>Mobile</a>
          <a>Ordinateur</a>
          <a>Tablette</a>
        </p>
        <!-- Search -->
        <div class=\"panel-block\">
          <p class=\"control has-icons-left\">
            <input class=\"input is-dark\" type=\"text\" placeholder=\"Search\" />
            <span class=\"icon is-left\">
              <i class=\"fas fa-search\" aria-hidden=\"true\"></i>
            </span>
          </p>
        </div>
        <!-- Saved items -->
        <div class=\"panel-block\" id=\"ads_delete_1\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ecran-casse-petit.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 6</span>
                    <br />
                    <span class=\"subtitle\">Lighting foutue</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete1()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_2\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ok3.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 5</span>
                    <br />
                    <span class=\"subtitle\">Ecran cassé</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 93
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete2()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_3\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone6.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 4</span>
                    <br />
                    <span class=\"subtitle\">Hors service</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 130
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete3()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_4\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/iphone4.png"), "html", null, true);
        echo "\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 6</span>
                    <br />
                    <span class=\"subtitle\">Lighting foutue</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"";
        // line 167
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("to_sell");
        echo "\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete4()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
      </article>
    </div>
  </div>
  <div class=\"columns\"></div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ads_saved.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  295 => 167,  277 => 152,  252 => 130,  234 => 115,  209 => 93,  191 => 78,  166 => 56,  148 => 41,  118 => 13,  108 => 12,  93 => 8,  83 => 7,  70 => 4,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %} 

{% block title %}
\t{{ parent() }} - Annonces sauvegardées
{% endblock %}

{% block navbar %}
{{ parent() }}
<div class=\"container-is-fluid extended\">
  <p class=\"title has-text-centered\">Annonces sauvegardées</p>
</div>
{% endblock %} {% block body %}
<div class=\"container is-fluid-without-padding p-3t\">
  <div class=\"columns is-centered\">
    <div class=\"column is-half\">
      <article class=\"panel\">
        <!-- Top of card -->
        <p class=\"panel-tabs\">
          <a class=\"is-active\">Tous</a>
          <a>Mobile</a>
          <a>Ordinateur</a>
          <a>Tablette</a>
        </p>
        <!-- Search -->
        <div class=\"panel-block\">
          <p class=\"control has-icons-left\">
            <input class=\"input is-dark\" type=\"text\" placeholder=\"Search\" />
            <span class=\"icon is-left\">
              <i class=\"fas fa-search\" aria-hidden=\"true\"></i>
            </span>
          </p>
        </div>
        <!-- Saved items -->
        <div class=\"panel-block\" id=\"ads_delete_1\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"{{ asset('img/ecran-casse-petit.png') }}\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 6</span>
                    <br />
                    <span class=\"subtitle\">Lighting foutue</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"{{ path(\"to_sell\") }}\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete1()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_2\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"{{ asset('img/ok3.png') }}\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 5</span>
                    <br />
                    <span class=\"subtitle\">Ecran cassé</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"{{ path(\"to_sell\") }}\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete2()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_3\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"{{ asset('img/iphone6.png') }}\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 4</span>
                    <br />
                    <span class=\"subtitle\">Hors service</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"{{ path(\"to_sell\") }}\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete3()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"panel-block\" id=\"ads_delete_4\">
          <div class=\"columns is-vcentered is-mobile\">
            <div class=\"column is-half is-offset-2-desktop is-offset-1-mobile\">
              <div class=\"columns\">
                <div class=\"column is-4 is-hidden-mobile\">
                  <figure class=\"image\">
                    <img
                      src=\"{{ asset('img/iphone4.png') }}\"
                      alt=\"image produit\"
                    />
                  </figure>
                </div>
                <div class=\"column is-8-desktop\">
                  <p>
                    <span class=\"title\">Iphone 6</span>
                    <br />
                    <span class=\"subtitle\">Lighting foutue</span>
                  </p>
                </div>
              </div>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"{{ path(\"to_sell\") }}\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"fas fa-search\"></i>
                </span>
              </a>
            </div>
            <div class=\"column is-1-desktop is-2-mobile\">
              <a href=\"#\" onclick=\"isDelete4()\">
                <span class=\"panel-icon is-size-5\">
                  <i class=\"far fa-trash-alt\"></i>
                </span>
              </a>
            </div>
          </div>
        </div>
      </article>
    </div>
  </div>
  <div class=\"columns\"></div>
</div>
{% endblock %}
", "ads_saved.html.twig", "/home/amaurel/dawin_plans/ufix/Ufix/templates/ads_saved.html.twig");
    }
}
