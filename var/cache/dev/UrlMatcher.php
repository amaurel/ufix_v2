<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home_page', '_controller' => 'App\\Controller\\DefaultController::showHomePage'], null, null, null, false, false, null]],
        '/home' => [[['_route' => 'home_connected', '_controller' => 'App\\Controller\\DefaultController::showConnectedHomePage'], null, null, null, false, false, null]],
        '/newad' => [[['_route' => 'new_ad', '_controller' => 'App\\Controller\\DefaultController::newAd'], null, null, null, false, false, null]],
        '/profil' => [[['_route' => 'profil_page', '_controller' => 'App\\Controller\\DefaultController::showProfilPage'], null, null, null, false, false, null]],
        '/repair' => [[['_route' => 'to_repair', '_controller' => 'App\\Controller\\DefaultController::showRepairPage'], null, null, null, false, false, null]],
        '/sell' => [[['_route' => 'to_sell', '_controller' => 'App\\Controller\\DefaultController::showSellPage'], null, null, null, false, false, null]],
        '/adsSaved' => [[['_route' => 'ads_saved', '_controller' => 'App\\Controller\\DefaultController::showAdsSaved'], null, null, null, false, false, null]],
        '/selectRepairer' => [[['_route' => 'select_repairer', '_controller' => 'App\\Controller\\DefaultController::showSelectRepairer'], null, null, null, false, false, null]],
        '/contact/toRepair' => [[['_route' => 'contact_repair', '_controller' => 'App\\Controller\\DefaultController::showContactRepair'], null, null, null, false, false, null]],
        '/contact/seller/buy' => [[['_route' => 'contact_seller_without_repair', '_controller' => 'App\\Controller\\DefaultController::showContactSellerBuy'], null, null, null, false, false, null]],
        '/contact/seller/repair' => [[['_route' => 'contact_seller_with_repair', '_controller' => 'App\\Controller\\DefaultController::showContactSellerWithRepair'], null, null, null, false, false, null]],
        '/contact/seller/repair-2' => [[['_route' => 'contact_seller_with_repair2', '_controller' => 'App\\Controller\\DefaultController::showContactSellerWithRepair2'], null, null, null, false, false, null]],
        '/messaging' => [[['_route' => 'messaging', '_controller' => 'App\\Controller\\DefaultController::showMessaging'], null, null, null, false, false, null]],
        '/edit-product' => [[['_route' => 'edit_product', '_controller' => 'App\\Controller\\DefaultController::showEditProduct'], null, null, null, false, false, null]],
        '/cgu' => [[['_route' => 'cgu', '_controller' => 'App\\Controller\\DefaultController::showCGU'], null, null, null, false, false, null]],
        '/contacter' => [[['_route' => 'contact', '_controller' => 'App\\Controller\\DefaultController::showContact'], null, null, null, false, false, null]],
        '/mentions_legales' => [[['_route' => 'legal_mentions', '_controller' => 'App\\Controller\\DefaultController::showLegalMentions'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
        159 => [
            [['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
